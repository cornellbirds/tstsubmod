<?php
/*
 *  Author: France Kehas-Dewaghe
 *  URL: allaboutbirds.org
 *  Custom functions, support, custom post types and more.
 */

/*------------------------------------*\
	Theme Support
\*------------------------------------*/
//testing again
if (!isset($content_width))
{
    $content_width = 900;
}

if (function_exists('add_theme_support'))
{
    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');
    add_image_size('x-large', 1920, '', array( 'center', 'top'));
    add_image_size('large', 1280, '', array( 'center', 'top' )); // Large Thumbnail
    add_image_size('medium', 720, '', array( 'center', 'top' )); // Medium Thumbnail
    add_image_size('small', 480, '', array( 'center', 'top' )); // Small Thumbnail
    add_image_size( 'thumbnail', 240, 135, array( 'center', 'top' ) ); //post thumbnail

    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('birdpress', get_template_directory() . '/languages');
}

/*------------------------------------*\
	Functions
\*------------------------------------*/

// Load Birdpress scripts (header.php)
function birdpress_header_scripts()
{
    if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

        wp_register_script('jquery', get_template_directory_uri() . '/js/min/jquery.min.js', array(), '1.11.2', true); // Modernizr
        wp_enqueue_script('jquery'); // Enqueue it!
        
        wp_register_script('modernizr', get_template_directory_uri() . '/js/vendor/modernizr-2.8.3.min.js', array(), '2.8.3', true); // Modernizr
        wp_enqueue_script('modernizr'); 

        wp_register_script('birdpressscripts', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0', true); // Custom scripts
        wp_enqueue_script('birdpressscripts'); 
        
        wp_register_script('birdpress-shortcode-script', get_template_directory_uri() . '/js/shortcode-functions.js', array('jquery'), '1.0.0', true);
        wp_enqueue_script('birdpress-shortcode-script');              
        
        wp_register_script('bootstrap', get_template_directory_uri() . '/js/min/bootstrap.min.js', array('jquery'), '2.8.3', true); // Modernizr
        wp_enqueue_script('bootstrap'); 
        
        wp_register_script('jplayer', get_template_directory_uri() . '/flat-audio/js/jquery.jplayer.min.js', array('jquery'),null, true); // Conditional script(s)
        wp_enqueue_script('jplayer','',null,'',true);
        
        wp_register_script('flat-audio', get_template_directory_uri() . '/flat-audio/js/flat.audio.min.js', array('jquery'),null, true); // Conditional script(s)
        wp_enqueue_script('flat-audio','',null,'',true);
    }
}

// Load Birdpress conditional scripts
function birdpress_conditional_scripts()
{
    if (is_page('pagenamehere')) {
        wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
        wp_enqueue_script('scriptname'); // Enqueue it!
    }
}

// Load Birdpress styles
function birdpress_styles()
{
    wp_register_style('font-awesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css',array(),'4.2.0','all');
    wp_enqueue_style('font-awesome');
    
    wp_register_style('flat-audio', get_template_directory_uri() . '/flat-audio/css/flat.audio.css',array(), null, 'all');
    wp_enqueue_style('flat-audio');

    //wp_register_style('birdpress', get_template_directory_uri() . '/css/main.min.css', array(), '1.0', 'all');
    //wp_enqueue_style('birdpress'); 
    
    wp_register_style('birdpress', get_template_directory_uri() . '/css/' . get_theme_mod('bp_color_scheme', 'theme-ORANGE') . '.min.css', array(), '1.0', 'all');
    wp_enqueue_style('birdpress');
}

//Load Birdpress phps
    require_once( dirname( __FILE__ ) . '/includes/bp-article-list.php');
    require_once( dirname( __FILE__ ) . '/includes/bp-custom-functions.php');
    require_once( dirname( __FILE__ ) . '/includes/bp-custom-taxonomies.php');
    require_once( dirname( __FILE__ ) . '/includes/bp-navs-menus-widgets.php');
    require_once( dirname( __FILE__ ) . '/includes/bp-post-fields.php');
    require_once( dirname( __FILE__ ) . '/includes/bp-rotating-banners.php');
    require_once( dirname( __FILE__ ) . '/includes/bp-short-codes.php' );
    require_once( dirname( __FILE__ ) . '/includes/bp-theme-customization.php');
    require_once( dirname( __FILE__ ) . '/includes/bp-simple-lists.php');

//show lab logo
if (! function_exists('birdpress_show_lab_logo') ){
    function birdpress_show_lab_logo(){
        return true;
    }
}

// Remove Injected classes, ID's and Page ID's from Navigation <li> items
function my_css_attributes_filter($var)
{
    return is_array($var) ? array() : '';
}

// Remove invalid rel attribute values in the categorylist
function remove_category_rel_from_category_list($thelist)
{
    return str_replace('rel="category tag"', 'rel="tag"', $thelist);
}


// Remove wp_head() injected Recent Comment styles
function my_remove_recent_comments_style()
{
    global $wp_widget_factory;
    remove_action('wp_head', array(
        $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
        'recent_comments_style'
    ));
}

// Custom Excerpts
function html5wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using html5wp_excerpt('html5wp_index');
{
    return 20;
}

// Create 40 Word Callback for Custom Post Excerpts, call using html5wp_excerpt('html5wp_custom_post');
function html5wp_custom_post($length)
{
    return 40;
}

// Remove Admin bar
function remove_admin_bar()
{
    return false;
}

// Remove 'text/css' from our enqueued stylesheet
function html5_style_remove($tag)
{
    return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
}

// Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
function remove_thumbnail_dimensions( $html )
{
    $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
    return $html;
}

// Custom Gravatar in Settings > Discussion
function birdpressgravatar ($avatar_defaults)
{
    $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
    $avatar_defaults[$myavatar] = "Custom Gravatar";
    return $avatar_defaults;
}

// Threaded Comments
function enable_threaded_comments()
{
    if (!is_admin()) {
        if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
            wp_enqueue_script('comment-reply');
        }
    }
}



/*------------------------------------*\
	Actions + Filters
\*------------------------------------*/

// Add Actions
add_action('init', 'birdpress_header_scripts'); // Add Custom Scripts to wp_head
add_action('wp_print_scripts', 'birdpress_conditional_scripts'); // Add Conditional Page Scripts
add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
add_action('wp_enqueue_scripts', 'birdpress_styles'); // Add Theme Stylesheet
add_action('init', 'register_birdpress_menu'); // Add Birdpress Menu
add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
//add_action('init', 'html5wp_pagination'); // Add our HTML5 Pagination

// Remove Actions
remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
remove_action('wp_head', 'index_rel_link'); // Index link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'rel_canonical');
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

// Add Filters
add_filter('avatar_defaults', 'birdpressgravatar'); // Custom Gravatar in Settings > Discussion
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)
//add_filter('wp_nav_menu_args', 'my_wp_nav_menu_args'); // Remove surrounding <div> from WP Navigation
add_filter('nav_menu_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected classes (Commented out by default)
// add_filter('nav_menu_item_id', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> injected ID (Commented out by default)
// add_filter('page_css_class', 'my_css_attributes_filter', 100, 1); // Remove Navigation <li> Page ID's (Commented out by default)
add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
add_filter('excerpt_more', 'birdpress_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
//add_filter('show_admin_bar', 'remove_admin_bar'); // Remove Admin bar
add_filter('style_loader_tag', 'html5_style_remove'); // Remove 'text/css' from enqueued stylesheet
add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images
add_filter('preprocess_comment','__return_false'); //Disable Wordpress Comments

// Remove Filters
remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

function bp_disable_attachment_list( $html ) { //Alter final html
    return '';
}
add_filter( 'wpatt_list_html', 'bp_disable_attachment_list' );

/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param    array  $plugins  
 * @return   array             Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Disable removal of html in category descriptions
 */
$filters = array('pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description');
foreach ( $filters as $filter ) {
    remove_filter($filter, 'wp_filter_kses');
}

/**
 * Add excerpts to pages
 */
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'page', 'excerpt' );
}

/**
 * Allow Editor Role to modify the theme and menus
 */
$role_object = get_role( 'editor' );
$role_object->add_cap( 'edit_theme_options' );

/**
 * Disable the Gutenburg Editor
 */
add_filter('use_block_editor_for_post', '__return_false');

?>
