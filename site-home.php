<?php
/**
 * Template Name: Birdpress Home Page Template
 *
 *
 */
get_header();
$banner_posts = get_posts(array(
        'posts_per_page' => -1,
        'post_type' => 'rotating-banners',
        'post_status' => 'publish'
    ));
if( sizeof($banner_posts) == 1 ) {
	$banner_post_images = get_posts(
        array(
            'post_parent' => $banner_posts[0]->ID,
            'post_type' => 'attachment',
            'posts_per_page' => -1,
            'post_status' => 'publish, inherit'
         )
    );
    if($banner_post_images) {
	   echo output_fullbleed_banner($banner_post_images[0]->guid, null, null, 'light narrower-height top-banner', apply_filters('the_content',$banner_posts[0]->post_content), null, null, get_posts(array('p' => $banner_post_images[0]->ID, 'post_type' => 'attachment'))[0]->post_excerpt, true);
    }
}
else if ($banner_posts AND sizeof($banner_posts) != 0){
	rotating_banners('rotating-banners');
}
?>
<div class="wrap content col-2 clearfix">

    <article class="grid" role="article">
    <?php
		if (have_posts()):
            while (have_posts()) :
                the_post();
                the_content();
            endwhile;
        endif;
    ?>
  </article><!-- end feature grid -->
    <aside class="sidebar">
    <div class="widget">
    <?php if(is_active_sidebar('home-page-widgets-side') && !dynamic_sidebar('home-page-widgets-side')) {}
    else if (!is_active_sidebar('home-page-widgets-side')) { }?>
    </div>
    </aside>
</div><!--end wrap -->

<?php get_footer();?>
