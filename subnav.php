<?php
$subnav_menu = get_post_meta( get_the_ID(), 'subnav-menu', true); 
if($subnav_menu) {
    if ( detect_mobile_browser() ) {
        ?>
        <div class="nav-area">
        <button class="btn " type="button" data-toggle="collapse" data-target="#bp-subnav-article" aria-expanded="false" aria-controls="bp-subnav-article"><?php echo $subnav_menu ?> <span class="fa fa-angle-down"></span></button>
        <div class='subnav-wrapper'><?php
        birdpress_nav($subnav_menu, 'top-three collapse', 'article');
        ?></div></div><?php                  
    }
    else {
        ?><div class='subnav-wrapper'><?php
        if(get_post_meta( get_the_ID(), 'show-subnav-menu-title', true) == '' ||
            get_post_meta(get_the_ID(), 'show-subnav-menu-title', true) == 'show'){
            echo "<h4>" . $subnav_menu . "</h4>";
        }
        birdpress_nav($subnav_menu, 'top-three', 'article');
        ?></div><?php                    
    }
}
?>