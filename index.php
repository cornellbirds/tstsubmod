<?php
get_header();
?>
<div class="wrap content col-2 clearfix">
	<article class="grid" role="article">
    <h1><?php _e( 'Latest Posts', 'birdpress' ); ?></h1>
        <?php
        set_query_var('bp-all-articles',true);
        get_template_part('loop');
		get_template_part('pagination', $wp_query->max_num_pages);
        ?>
    </article>
    <aside class="sidebar">
    <?php get_sidebar()?>
    </aside>
</div>
<?php get_template_part('next-prev'); ?>
<?php get_template_part('comment-area'); ?>
<?php get_footer();?>
