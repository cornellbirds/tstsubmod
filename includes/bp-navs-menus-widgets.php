<?php

// Register Custom Navigation Walker
require_once( dirname( __FILE__ ) . '/wp_bootstrap_navwalker.php');

if (! function_exists('birdpress_nav') ) {
    function birdpress_nav($t, $menu_class, $menu_html_id)
    {
        wp_nav_menu( array(
            'menu'              => $t,
            'theme_location'    => $t,
            'depth'             => 0,
            'container'         => 'div',
            'container_class'   => 'row',
            'container_id'      => '',
            'menu_class'        => 'nav navbar-nav navbar-default ' . $menu_class,
            'fallback_cb'       => 'wp_bootstrap_navwalker::fallback',
            'items_wrap'      => '<ul id="bp-subnav-' . $menu_html_id . '" class="%2$s">%3$s</ul>',
            'walker'            => new wp_bootstrap_navwalker())
        );
    }
}

// interactives nav
if( ! function_exists('birdpress_interactives_nav' ) ) {
    function birdpress_interactives_nav()
    {
        ?>
        <div class="nav">
            <div id="enewsLink">
                <a href="#">Get eNews</a>
            </div>
            <div id="contactUsLink">
                <a href="#">Contact Us</a>
            </div>
            <div id="searchLink">
                <a href="#">Search</a>
            </div>
        </div>
        <?php
    }
}

if(! function_exists('birdpress_sitemap') ) {
    function birdpress_sitemap($location_slug)
    {
        wp_nav_menu( array(
            'menu' => $location_slug,
            'theme_location' => $location_slug,
            'container'       => 'div',
            'container_class' => '',
            'container_id'    => '',
            'menu_class'      => 'menu',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => '',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
            ));
    }
}

if(! function_exists('birdpress_footer') ) {
    function birdpress_footer($location_slug)
    {
        wp_nav_menu( array(
            'menu' => $location_slug,
            'theme_location' => $location_slug,
            'container'       => 'div',
            'container_class' => '',
            'container_id'    => '',
            'menu_class'      => 'menu',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => '',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul class="contact-list">%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
            ));
    }
}

// Widgets
if (function_exists('register_sidebar'))
{
    // Define Home Page Center Widget Area
    // register_sidebar(array(
    //     'name' => __('Home Page Widgets - Center', 'birdpress'),
    //     'description' => __('Center Content on Home Page', 'birdpress'),
    //     'id' => 'home-page-widgets-center',
    //     'before_widget' => '',
    //     'after_widget' => '',
    //     'before_title' => '',
    //     'after_title' => ''
    // ));

    // Define Home Page Side Widget Area
    register_sidebar(array(
        'name' => __('Home Page Widgets - Side', 'birdpress'),
        'description' => __('Sidebar Content on Home Page', 'birdpress'),
        'id' => 'home-page-widgets-side',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));

    // Define Side Widget Area
    register_sidebar(array(
        'name' => __('Birdpress Widgets - Side', 'birdpress'),
        'description' => __('Sidebar Content on Birdpress', 'birdpress'),
        'id' => 'birdpress-widgets-side',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));

    register_sidebar(array(
        'name' => __('Enews Signup', 'birdpress'),
        'description' => __('Sidebar Content on Birdpress', 'birdpress'),
        'id' => 'birdpress-enews-signup',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));

    // Define Footer Widget Area
    register_sidebar(array(
        'name' => __('Footer', 'birdpress'),
        'description' => __('', 'birdpress'),
        'id' => 'custom-footer-widget',
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '',
        'after_title' => ''
    ));

}

if(! function_exists('register_birdpress_menu') ) {
    function register_birdpress_menu()
    {
        register_nav_menus(array( // Using array to specify more menus if needed
            'header-menu' => 'Header Menu',
            'footer-menu' => 'Footer Menu',
            'site-map'    => 'Site Map'
        ));
    }
}
?>
