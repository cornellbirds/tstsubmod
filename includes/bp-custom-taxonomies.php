<?php
/**
 * Custom Taxonomies used for Birdpress Posts
 */

//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_content_format_hierarchical_taxonomy', 0 );

if ( ! function_exists('create_content_format_hierarchical_taxonomy') ) {
  /**
   * [create_content_format_hierarchical_taxonomy - create custom taxonomy for content format]
   * @return [void]
   */
  function create_content_format_hierarchical_taxonomy() {   
      $labels = array(
        'name' => _x( 'Content Format', 'taxonomy general name' ),
        'singular_name' => _x( 'Content Format', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Content Formats'),
        'all_items' => __( 'All Content Formats' ),
        'parent_item' => __( 'Parent Content Format' ),
        'parent_item_colon' => __( 'Parent Content Format:' ),
        'edit_item' => __( 'Edit Content Format' ),
        'update_item' => __( 'Update Content Format' ),
        'add_new_item' => __( 'Add New Content Format' ),
        'new_item_name' => __( 'New Content Format' ),
        'menu_name' => __( 'Content Formats' ),
      );
      register_taxonomy('content-format',array('post'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'content-format' )
      ));
  }
}

/**
 * [get_content_format_class - output content format in order of Video > Download > Audio > Slideshow > Article
 * used for css styling]
 * @param  [array] $terms [taxonomy terms assigned]
 * @return [string]        [string of the taxonomy terms assigned]
 */
if(! function_exists('get_content_format_class') ) {
  function get_content_format_class( $terms ) {
      $retval = "";
      if(sizeof($terms) < 1){
          $retval = "content-article";
      }
      else{
          foreach($terms as $t)
          {
              $retval = $retval . " content-" . str_replace(" ","-",strtolower($t));
          }
      }
      return $retval;
  }
}
?>