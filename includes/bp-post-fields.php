<?php
function prfx_custom_meta() {
    add_meta_box( 'prfx_meta_banner_image', __( 'Banner Image/Video', 'prfx-textdomain' ), 'prfx_meta_callback__banner_image', array('post','page') );
    add_meta_box( 'prfx_meta_callback__banner_text_style', __( 'Banner Text Style', 'prfx-textdomain' ), 'prfx_meta_callback__banner_text_style', array('post','page') );
    add_meta_box( 'prfx_meta_banner_youtube', __( 'Banner Youtube Link', 'prfx-textdomain' ), 'prfx_meta_callback__banner_youtube', array('post','page') );
    add_meta_box( 'prfx_meta_custom_byline', __( 'Custom Byline', 'prfx-textdomain' ), 'prfx_meta_callback__custom_byline', array('post','page') );
    add_meta_box( 'prfx_meta_external_link', __( 'External Link', 'prfx-textdomain' ), 'prfx_meta_callback__external_link', 'post' );
    add_meta_box( 'prfx_meta_display_featured_image', __( 'Display Featured Image', 'prfx-textdomain' ), 'prfx_meta_callback__display_featured_image', 'post', 'side' );
    add_meta_box( 'prfx_meta_display_featured_image', __( 'Display Featured Image', 'prfx-textdomain' ), 'prfx_meta_callback__display_featured_image', 'page', 'side' );
    add_meta_box( 'prfx_meta_next_article', __( 'Next/Previous Articles', 'prfx-textdomain' ), 'prfx_meta_callback__next_article', 'post' );
    add_meta_box( 'prfx_meta_next_article', __( 'Next/Previous Articles', 'prfx-textdomain' ), 'prfx_meta_callback__next_article', 'page' );
    add_meta_box( 'prfx_meta_subnav_menu', __( 'Subnav Menu', 'prfx-subnav' ), 'prfx_meta_callback__subnav_menu', 'post' );
    add_meta_box( 'prfx_meta_subnav_menu', __( 'Subnav Menu', 'prfx-subnav' ), 'prfx_meta_callback__subnav_menu', 'page' );
    add_meta_box( 'prfx_meta_show_date', __( 'Show Date', 'prfx-textdomain' ), 'prfx_meta_callback__show_date', array('post','page') );
    add_meta_box( 'prfx_meta_post_format', __( 'Post Layout', 'prfx-textdomain' ) , 'prfx_meta_callback__post_format', 'post');
}
add_action( 'add_meta_boxes', 'prfx_custom_meta' );

/**
 * Outputs the content of the meta box
 */


function prfx_meta_callback__banner_image( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'prfx_nonce' );
    $prfx_stored_meta = get_post_meta( $post->ID );
    ?>
    <p>NOTE: This option only works One-Column Template Pages and Posts
    </p>
    <p>
        <label for="banner-image" class="prfx-row-title"><?php _e( 'Select Banner Image', 'prfx-textdomain' )?></label>
        <input type="text" name="banner-image" id="banner-image" value="<?php if ( isset ( $prfx_stored_meta['banner-image'] ) ) echo $prfx_stored_meta['banner-image'][0]; ?>" />
        <input type="button" id="banner-image-button" class="button" value="<?php _e( 'Choose or Upload an Image/Video', 'prfx-textdomain' )?>" />
        <br/>
        <label for="fallback-videobanner-image" class="prfx-row-title"><?php _e( 'Fallback Video Image', 'prfx-textdomain' )?></label>
        <input type="text" name="fallback-videobanner-image" id="fallback-videobanner-image" value="<?php if ( isset ( $prfx_stored_meta['fallback-videobanner-image'] ) ) echo $prfx_stored_meta['fallback-videobanner-image'][0]; ?>" />
        <input type="button" id="fallback-videobanner-image-button" class="button" value="<?php _e( 'Choose or Upload an Image', 'prfx-textdomain' )?>" />
        <br/>
        <label for="banner-image-caption" class="prfx-row-title"><?php _e( 'Enter Banner Caption', 'prfx-textdomain' )?></label>
        <input type="text" name="banner-image-caption" id="banner-image-caption" value="<?php if (isset ( $prfx_stored_meta['banner-image-caption'] ) ) echo $prfx_stored_meta['banner-image-caption'][0]; ?>"/>
        <br/>
        <img name="banner-image-preview" id="banner-image-preview" src="<?php if ( isset ( $prfx_stored_meta['banner-image'] ) ) echo $prfx_stored_meta['banner-image'][0]; ?>" style="width:700px; padding-top:25px"/>
    </p>
    <?php
}

function prfx_meta_callback__display_featured_image($post){
    wp_nonce_field( basename( __FILE__ ), 'prfx_nonce' );
    $prfx_stored_meta = get_post_meta( $post->ID );
    $selected = isset( $prfx_stored_meta['featured-image-display'] ) ? esc_attr( $prfx_stored_meta['featured-image-display'][0] ) : 'yes';
    ?>
    <p>
        <label for="featured-image-display" class="prfx-row-title"><?php _e( 'Display Featured Image', 'prfx-textdomain' )?></label>
        <select id="featured-image-display" name="featured-image-display">
            <option <?php selected( $selected, 'yes' ); ?>>yes</option>
            <option <?php selected( $selected, 'no' ); ?>>no</option>
        </select>
    </p>
    <?php
}

function prfx_meta_callback__post_format ( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'prfx_nonce' );
    $prfx_stored_meta = get_post_meta( $post->ID );
    $selected = (get_post_meta($post->ID,'bp-post-format',true)) ? get_post_meta($post->ID,'bp-post-format',true) : 'col-2';
    ?>
    <p>
        <label for="bp-post-format" class="prfx-row-title"><?php _e( 'Post Format', 'prfx-textdomain' )?></label>
        <select id="bp-post-format" name="bp-post-format">
            <option value='col-1' <?php selected( $selected, 'col-1' ); ?>>One Column</option>
            <option value='col-2' <?php selected( $selected, 'col-2' ); ?>>Two Column</option>
        </select>
    </p>
    <?php
}

function prfx_meta_callback__banner_text_style( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'prfx_nonce' );
    $prfx_stored_meta = get_post_meta( $post->ID );
    $selected = isset( $prfx_stored_meta['banner-text-style'] ) ? esc_attr( $prfx_stored_meta['banner-text-style'][0] ) : 'light';
    ?>
    <p>NOTE: This option only works One-Column Template Pages and Posts
    </p>
    <p>
        <label for="banner-text-style" class="prfx-row-title"><?php _e( 'Select Banner Text Style', 'prfx-textdomain' )?></label>
        <select id="banner-text-style" name="banner-text-style">
            <option <?php selected( $selected, 'light' ); ?>>light</option>
            <option <?php selected( $selected, 'dark' ); ?>>dark</option>
        </select>
    </p>
    <?php
}

function prfx_meta_callback__show_date ( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'prfx_nonce' );
    $prfx_stored_meta = get_post_meta( $post->ID );
    $selected = isset( $prfx_stored_meta['bp-show-date'] ) ? esc_attr( $prfx_stored_meta['bp-show-date'][0] ) : 'yes';
    ?>
    <p>
        <label for="bp-show-date" class="prfx-row-title"><?php _e( 'Show Post Date?', 'prfx-textdomain' )?></label>
        <select id="bp-show-date" name="bp-show-date">
            <option <?php selected( $selected, 'yes' ); ?>>yes</option>
            <option <?php selected( $selected, 'no' ); ?>>no</option>
        </select>
    </p>
    <?php
}

function prfx_meta_callback__next_article( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'prfx_nonce' );
    $prfx_stored_meta = get_post_meta( $post->ID );
    ?>
    <p>
        <label for="previous-article" class="prfx-row-title"><?php _e( 'Previous Article ID', 'prfx-textdomain' )?></label>
        <input type="text" name="previous-article" id="previous-article" style="width:100px" value="<?php if ( isset ( $prfx_stored_meta['previous_article'] ) ) echo $prfx_stored_meta['previous_article'][0]; ?>" />
    </p>
    <p>
        <label for="next-article" class="prfx-row-title"><?php _e( 'Next Article ID', 'prfx-textdomain' )?></label>
        <input type="text" name="next-article" id="next-article" style="width:100px" value="<?php if ( isset ( $prfx_stored_meta['next_article'] ) ) echo $prfx_stored_meta['next_article'][0]; ?>" />
    </p>
    <?php
}

function prfx_meta_callback__banner_youtube( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'prfx_nonce' );
    $prfx_stored_meta = get_post_meta( $post->ID );
    ?>
    <p>NOTE: This option only works One-Column Template Pages and Posts
    </p>
    <p>
        <label for="banner-youtube" class="prfx-row-title"><?php _e( 'Enter Youtube Video ID', 'prfx-textdomain' )?></label>
        <input type="text" name="banner-youtube" id="banner-youtube" style="width:700px" value="<?php if ( isset ( $prfx_stored_meta['banner-youtube'] ) ) echo $prfx_stored_meta['banner-youtube'][0]; ?>" />
    </p>
    <?php
}

function prfx_meta_callback__external_link( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'prfx_nonce' );
    $prfx_stored_meta = get_post_meta( $post->ID );
    ?>
    <p>
        <label for="external-link" class="prfx-row-title"><?php _e( 'External Link for Article', 'prfx-textdomain' )?></label>
        <input type="text" name="external-link" id="external-link" style="width:700px" value="<?php if ( isset ( $prfx_stored_meta['original_guid'] ) ) echo $prfx_stored_meta['original_guid'][0]; ?>" />
        <br/><em>make sure to use full link (http:// etc) ---</em>
    </p>
    <?php
}

function prfx_meta_callback__custom_byline( $post ) {
    wp_nonce_field( basename( __FILE__ ), 'prfx_nonce' );
    $prfx_stored_meta = get_post_meta( $post->ID );
    ?>
    <p>
        <?php wp_editor($prfx_stored_meta['custom-byline'][0], 'custom-byline');?>
    </p>
    <?php
}

function prfx_meta_callback__subnav_menu( $post ) {
    wp_nonce_field( basename(__FILE__ ), 'prfx_nonce' );
    $prfx_stored_meta = get_post_meta( $post->ID );
    $selected = isset( $prfx_stored_meta['subnav-menu'] ) ? esc_attr( $prfx_stored_meta['subnav-menu'][0] ) : '';
    $selected_show_title = isset($prfx_stored_meta['show-subnav-menu-title']) ? esc_attr($prfx_stored_meta['show-subnav-menu-title'][0]):'show';
    $menus = get_terms( 'nav_menu', array( 'hide_empty' => true ) );
    ?>
    <p>
    <label for="subnav-menu" class="prfx-row-title"><?php _e( 'Subnav Menu', 'prfx-textdomain' )?></label>
    <select id="subnav-menu" name="subnav-menu">
    <option <?php selected($selected,'');?> value=''>Select</option>
    <?php
    foreach ( $menus as $menu ):
    ?>
	    <option <?php selected ($selected, $menu->name); ?>><?php echo $menu->name ?></option>
    <?php endforeach; ?>
    </select>
    </p>
    <p>
    <label for='show-subnav-menu-title' class='prfx-row-title'><?php _e('Show Subnav Menu Title', 'prfx-textdomain')?></label>
    <select id='show-subnav-menu-title' name='show-subnav-menu-title'>
    <option <?php selected($selected_show_title, 'show'); ?>>show</option>
    <option <?php selected($selected_show_title, 'hide'); ?>>hide</option>
    </select>
    </p>
    <?php
}


/**
 * Loads the image management javascript
 */
function prfx_image_enqueue() {
    global $typenow;
    if( $typenow == 'post' || $typenow == 'page' ) {
        wp_enqueue_media();

        // Registers and enqueues the required javascript.
        wp_register_script( 'meta-box-image', get_template_directory_uri() . '/js/meta-box-image.js', array( 'jquery' ) );
        wp_localize_script( 'meta-box-image', 'meta_image',
            array(
                'title' => __( 'Choose or Upload an Image', 'prfx-textdomain' ),
                'button' => __( 'Use this image', 'prfx-textdomain' ),
            )
        );
        wp_enqueue_script( 'meta-box-image' );
    }
}
add_action( 'admin_enqueue_scripts', 'prfx_image_enqueue' );

/**
 * Saves the custom meta input
 */
function prfx_meta_save( $post_id ) {

    // Checks save status
    $is_autosave = wp_is_post_autosave( $post_id );
    $is_revision = wp_is_post_revision( $post_id );
    $is_valid_nonce = ( isset( $_POST[ 'prfx_nonce' ] ) && wp_verify_nonce( $_POST[ 'prfx_nonce' ], basename( __FILE__ ) ) ) ? 'true' : 'false';

    // Exits script depending on save status
    if ( $is_autosave || $is_revision || !$is_valid_nonce ) {
        return;
    }

    // Checks for input and sanitizes/saves if needed
    if( isset( $_POST[ 'custom-byline' ] ) ) {
        update_post_meta( $post_id, 'custom-byline', $_POST[ 'custom-byline' ] );
    }
    if( isset( $_POST[ 'banner-youtube' ] ) ) {
        update_post_meta( $post_id, 'banner-youtube', sanitize_text_field( $_POST[ 'banner-youtube' ] ) );
    }
    if( isset( $_POST[ 'banner-image' ] ) ) {
        update_post_meta( $post_id, 'banner-image', $_POST[ 'banner-image' ] );
    }
    if( isset( $_POST[ 'banner-image-caption' ] )) {
        update_post_meta( $post_id, 'banner-image-caption', esc_attr($_POST[ 'banner-image-caption' ] ));
    }
    if( isset( $_POST[ 'fallback-videobanner-image' ] ) ) {
        update_post_meta( $post_id, 'fallback-videobanner-image', $_POST[ 'fallback-videobanner-image' ] );
    }
    if( isset( $_POST[ 'external-link' ] ) ) {
        update_post_meta( $post_id, 'original_guid', $_POST[ 'external-link' ] );
    }
    if( isset( $_POST[ 'banner-text-style' ] ) ) {
        update_post_meta( $post_id, 'banner-text-style', esc_attr( $_POST['banner-text-style'] ) );
    }
    if( isset( $_POST[ 'featured-image-display' ] ) ) {
        update_post_meta( $post_id, 'featured-image-display', esc_attr( $_POST['featured-image-display'] ) );
    }
    if( isset( $_POST[ 'next-article' ] ) ) {
        update_post_meta( $post_id, 'next_article', esc_attr( $_POST['next-article'] ) );
    }
    if( isset( $_POST[ 'previous-article' ] ) ) {
        update_post_meta( $post_id, 'previous_article', esc_attr( $_POST['previous-article'] ) );
    }
    if( isset( $_POST['subnav-menu'] ) ) {
        update_post_meta( $post_id, 'subnav-menu', esc_attr( $_POST['subnav-menu'] ) );
    }
    if( isset( $_POST['show-subnav-menu-title'] ) ) {
        update_post_meta( $post_id, 'show-subnav-menu-title', esc_attr( $_POST['show-subnav-menu-title'] ) );
    }
    if( isset( $_POST[ 'bp-show-date' ] ) ) {
        update_post_meta( $post_id, 'bp-show-date', esc_attr( $_POST['bp-show-date'] ) );
    }
    if( isset( $_POST[ 'bp-post-format' ] ) ) {
        update_post_meta( $post_id, 'bp-post-format', esc_attr( $_POST['bp-post-format'] ) );
    }
}
add_action( 'save_post', 'prfx_meta_save' );

// Add Buttons to Gallery Settings //
//_________________________________//
add_action('print_media_templates', function(){

  // define your backbone template;
  // the "tmpl-" prefix is required,
  // and your input field should have a data-setting attribute
  // matching the shortcode name
  ?>
  <script type="text/html" id="tmpl-my-custom-gallery-setting">
    <label class="setting">
      <span><?php _e('Max Width'); ?></span>
      <input type="text" data-setting="custom_maxwidth">
    </label>
    <label class="setting">
      <span><?php _e('Position'); ?></span>
      <input type="text" data-setting="position">
    </label>
    <label class="setting">
        <span><?php _e('Height'); ?></span>
        <input type="text" data-setting="height">
    </label>
  </script>


  <script>

    jQuery(document).ready(function(){

      // add your shortcode attribute and its default value to the
      // gallery settings list; $.extend should work as well...
      _.extend(wp.media.gallery.defaults, {
        custom_maxwidth: 'default_val'
      });

      // merge default gallery settings template with yours
      wp.media.view.Settings.Gallery = wp.media.view.Settings.Gallery.extend({
        template: function(view){
          return wp.media.template('gallery-settings')(view)
               + wp.media.template('my-custom-gallery-setting')(view);
        }
      });

    });

  </script>
  <?php

});
?>
