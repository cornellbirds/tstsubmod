<?php
/*------------------------------------*\
	ShortCode Functions
\*------------------------------------*/
if ( ! function_exists('disqus_shortcode') ) {
    add_shortcode('disqus', 'disqus_shortcode');
    function disqus_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "shortname" => ""), $atts));
        $script_to_add = "var disqus_config = function () {" .
            "    this.page.url = '". get_the_permalink() . "';" .
            "    this.page.identifier = '" . get_the_ID() . "';" .
            "};" .
            "(function() {" .
            "    var d = document, s = d.createElement('script');" .
            "    s.src = 'https://" . $shortname . ".disqus.com/embed.js';" .
            "    s.setAttribute('data-timestamp', +new Date());" .
            "    (d.head || d.body).appendChild(s);" .
            "})();";
        wp_enqueue_script('script-typed', get_template_directory_uri() . '/js/main.js', array('jquery'));
        wp_add_inline_script ('script-typed', $script_to_add);
        return "<div id=\"disqus_thread\"></div>";
    }
}
if ( ! function_exists( 'article_list' ) ) {
    add_shortcode('widget_article_list', 'article_list');
    add_shortcode('article_list', 'article_list');
    function article_list($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "id" => null, //id of the article list
            "post_ids" => null, //list of post ids
            "class" => 'list-style', //class of article list
            "menu" => null, //name of menu of posts
            "title" => null, //title of article list
            "excerpts" => null, //true or false(or null) to show excerpts
            "att_top" => null, //name of field to use for top attribution
            "att_bottom" => null, //name of field to use fo bottom attribution
            "category" => null,  //category of posts to display
            "show_sort" => false,
            "paginate" => false,
            "max_items" => 12,
            "excerpt_length" => 55,
            "show_images" => 'true',
            "sortorder"   => 'desc', //ASC or DESC
            "sortby"      => 'date',    //title or date
            "parent_id"    => null, //id of a page used to output all child pages
            "show_title"    => 'true',
            "carousel" => false,
            "slick_class" => '',
            "see_more_max" => null,
            "link" => true
        ), $atts));
        $articleList = array();
        $max_num_pages = 0;
        $retSortString = "";
        if($menu != '') {
            $articleList = convert_wordpress_menu_to_article_list($menu);
            $max_num_pages = sizeof($articleList)/$max_items;
        }
        else if($parent_id) {
            if($sortby == 'date') {
                $sortby = 'post_date';
            } else if ($sortby == 'title'){
                $sortby = 'post_title';
            }
            $all_pages = get_pages( array(
            'post_type' => 'page',
            'post_status' => 'publish',
            'parent' => $parent_id,
            'sort_order' => $sortorder,
            'sort_column' => $sortby,
            'numberposts' => $max_items,
            'posts_per_page' => $max_items
            ) );
            foreach($all_pages as $page){
                $listItem = build_ArticleItem($page->ID);
                array_push($articleList, $listItem);
            }
            $number_of_articles = sizeof(get_pages(array (
                'post_type' => 'page',
                'post_status' => 'publish',
                'parent' => $parent_id
                )))/$max_items;
        }
        else if($category){
            if($show_sort) {
                $retSortString = birdpress_output_sortbox('dropdown-menu dropdown-menu-right');
            }
            $sort_order_by = (get_query_var('orderby')) ? get_query_var('orderby') : $sortby;
            $sort_order = (get_query_var('order')) ? get_query_var('order') : $sortorder;
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            if($paginate){
                $args = array('category_name' => $category, 'order' => $sortorder, 'orderby' => $sortby, 'paged' => $paged, 'numberposts' => $max_items, 'posts_per_page' => $max_items, 'offset' => (($paged - 1) * $max_items),'post_type' => 'post', 'post_status' => 'publish');
            }
            else{
                $args = array('category_name' => $category, 'order' => $sortorder, 'orderby' => $sortby, 'numberposts' => $max_items, 'posts_per_page' => $max_items,'post_type' => 'post', 'post_status' => 'publish');
            }
            $wp_query2 = new WP_Query( $args );
            if ($wp_query2->have_posts()):
                while ($wp_query2->have_posts()) :
                    $wp_query2->the_post();
                    $listItem = build_ArticleItem(get_the_ID());
                    array_push($articleList, $listItem);
                endwhile;
            endif;
            $max_num_pages = $wp_query2->max_num_pages;
            wp_reset_query();
        }
        else if($post_ids) {
            $postIdArray = explode(",",$post_ids);
            foreach($postIdArray as $postId){
                $listItem = build_ArticleItem($postId);
                array_push($articleList, $listItem);
            }
            $max_num_pages = sizeof($postIdArray)/$max_items;
        }
        if(sizeof($articleList) > 0) {
            $retSortString .= output_article_list($id,$class, $title, $articleList, $excerpts, $att_top, $att_bottom, $show_images, $show_title, $carousel, $slick_class, $see_more_max, $link);
            if($paginate) {
                $retSortString .= "<div class=\"btn-pagination\">" . birdpress_pagination($max_num_pages) . "</div>";
            }
            return $retSortString;
        }
        else {
            return "<h3>No articles specified, either give a list of ids or a menu id</h3>";
        }
    }
}

if( ! function_exists( 'widget_single_image' ) ) {
    add_shortcode('widget_single_image', 'widget_single_image');
    function widget_single_image($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "post_id" => null, //id of article to use, featured image is image
            "image_size" => 'large', //specify which image size
            "class" => null
            ), $atts));
        ?>
        <?php

        if(isset($post_id)) {
            $retString = '';
            $retString .= "<div class=\"hero-large ";
            if(isset($class)) { $retString .= $class; }
            $retString .= "\" style=\"background:url(" .  wp_get_attachment_image_src(get_post_thumbnail_id($post_id), $image_size)[0] . ") no-repeat center center\">" .
            birdpress_get_the_content($post_id) . '</div>';
            return $retString;
        }
        else {
            return "<h3>ERROR: Invalid widget_single_image shortcode, set post_id</h3>";
        }
    }
}

if( ! function_exists( 'slideshow_shortcode' ) ) {
    add_shortcode('gallery','slideshow_shortcode');
    function slideshow_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "ids" => '1',
            "custom_maxwidth" => '',
            "size" => '',
            "position" => '',
            "interval" => '8000',
            "height" => ''
        ), $atts));
        //switch wordpress size's for ours
        if ($size == 'full') {
            $size = 'full';
            $sizeClass = 'large';
        }
        else if($size == 'large')
        {
            $sizeClass = 'large';
        }
        else if($size == 'medium')
        {
            $sizeClass = 'medium';
        }
        else if($size == '')
        {
            $size = 'small';
            $sizeClass = 'small';
        }
        if($ids == 1){
            return "<div class=\"shortcode-error\">INVALID SLIDESHOW/GALLERY SHORTCODE</div>";
        }
        else{
            $imgArray = explode(",",$ids);

            if(count($imgArray) == 1)
            {
                return "<div class=\"photo " . $sizeClass . "\">".
                            aab_get_image_tag( $imgArray[0],$size ) .
                       "<small>" . get_posts(array('p' => $imgArray[0], 'post_type' => 'attachment'))[0]->post_excerpt . "</small></div>";
            }

            else if(count($imgArray) == 2)
            {
                return "<div class=\"photo two-up\"><div class=\"left\">" . aab_get_image_tag( $imgArray[0],$size ) . "<small>" . get_posts(array('p' => $imgArray[0], 'post_type' => 'attachment'))[0]->post_excerpt . "</small></div>" .
                "<div class=\"right\">" . aab_get_image_tag( $imgArray[1],$size ) . "<small>" . get_posts(array('p' => $imgArray[1], 'post_type' => 'attachment'))[0]->post_excerpt . "</small></div></div>";
            }


            else{

                //generate show_id slideshow unique on the page
                $show_id = rand(1,10000);
                $retstr = "";
                $active = "active";
                $count = 0;
                $indicatorString = "";
                $itemString = "";

                $maxWidth = ($custom_maxwidth!="") ? "max-width:".$custom_maxwidth."px" : "";
                $heightStyle = ($height!="") ? "style=\"min-height:" . $height . "\"" : "";
                foreach($imgArray as $img)
                {
                    $thumbnail_image = get_posts(array('p' => $img, 'post_type' => 'attachment'));
                    $indicatorString = $indicatorString . "<li data-target=\"#carousel".$show_id."\" data-slide-to=\"" . $count . "\" class=\"" . $active . "\"></li>";
                    $itemString = $itemString . "<div class=\"item " . $active . "\">" . aab_get_image_tag( $img,$size ) . "<div class=\"carousel-caption\" ".$heightStyle."><small>" . $thumbnail_image[0]->post_excerpt . "</small></div></div>";
                    $active = "";
                    $count = $count + 1;
                }
                return "<div id=\"carousel".$show_id."\" class=\"carousel slide ". $sizeClass . " " .$position."\" style=\"" . $maxWidth . "\" data-ride=\"carousel\" data-interval=\"".$interval."\"><ol class=\"carousel-indicators\">" .
                    $indicatorString . "</ol>" .
                    "<div class=\"carousel-inner\" role=\"listbox\">" .
                    $itemString . "</div>" .
                    "<a class=\"left carousel-control\" href=\"#carousel".$show_id."\" role=\"button\" data-slide=\"prev\">" .
                    "<span class=\"glyphicon glyphicon-chevron-left\" aria-hidden=\"true\"></span>" .
                    "<span class=\"sr-only\">Previous</span>" .
                    "</a>" .
                    "<a class=\"right carousel-control\" href=\"#carousel".$show_id."\" role=\"button\" data-slide=\"next\">" .
                    "<span class=\"glyphicon glyphicon-chevron-right\" aria-hidden=\"true\"></span>" .
                    "<span class=\"sr-only\">Next</span>" .
                    "</a>" .
                    "</div>";
            }
        }
    }
}
if ( ! function_exists( 'caption_shortcode' ) ) {
    add_shortcode('caption','caption_shortcode');
    function caption_shortcode( $attr, $content = null ) {
        // New-style shortcode with the caption inside the shortcode with the link and image tags.
        if ( ! isset( $attr['caption'] ) ) {
            if ( preg_match( '#((?:<a [^>]+>\s*)?<img [^>]+>(?:\s*</a>)?)(.*)#is', $content, $matches ) ) {
                $content = $matches[1];
                $attr['caption'] = trim( $matches[2] );
            }
        }
        $linkUrl = "";
        if ( preg_match( '~<a\s+(?:[^>]*?\s+)?href="([^"]*)"~', $content, $m ) ){
            $linkUrl = $m[1];
        }

        $atts = shortcode_atts( array(
            'id'	  => '',
            'align'	  => '',
            'width'	  => '',
            'caption' => '',
            'class'   => ''
        ), $attr, 'caption' );

        if($atts['width'] < 300)
        {
            $size = "small";
        }
        else if($atts['width'] < 500)
        {
            $size = "medium";
        }
        else if($atts['width'] < 720)
        {
            $size = "medium";
        }
        else
        {
            $size = "large";
        }
        if($linkUrl == '')
        {
            return "<div class=\"photo " . $size. " " . $atts['align'] .  "\">" . aab_get_image_tag( str_replace("attachment_","",$atts['id']),$size ) . "<small>" . do_shortcode($atts['caption']) . "</small>" . "</div>";
        }
        else
        {
            return "<div class=\"photo " . $size .  " " . $atts['align'] . "\"><a href=\"" . $linkUrl . "\">" . aab_get_image_tag( str_replace("attachment_","",$atts['id']),$size ) . "</a><small>" . do_shortcode($atts['caption']) . "</small>" . "</div>";
        }
    }
}

if( ! function_exists('full_width_shortcode') ) {
    add_shortcode('full_width','full_width_shortcode');
    function full_width_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "path" => "",
            //"height" => "500px",
            "class" => "light",
            "text_overlay" => ""
            ), $atts));
        if($path == ""){
            return "<div class=\"shortcode-error\">INVALID FULL WIDTH SHORTCODE</div>";
        }
        else{
            $class = $class . ' content-banner';
            return output_fullbleed_banner($path,null,null,$class,$text_overlay,null,null,$content, $false);
        }
    }
}

if( ! function_exists('full_width_video_shortcode') ) {
    add_shortcode('full_width_video','full_width_video_shortcode');
    function full_width_video_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            //"height" => "500px",
            "path" => "",
            "poster" => "",
            "text_overlay" => "",
            "class" => ""
            ), $atts));
        if($path == ""){
            return "<div class=\"shortcode-error\">INVALID FULL WIDTH VIDEO SHORTCODE</div>";
        }
        else if ($poster == "")
        {
            return "<div class=\"shortcode-error\">INVALID FULL WIDTH VIDEO SHORTCODE -- MISSING poster</div>";
        }
        else{
            $class = $class . ' content-banner';
            return output_fullbleed_banner($poster,$path,null,$class,$text_overlay,null,null,$content, false);
        }
    }
}

if ( ! function_exists('offset_sidebar_shortcode') ) {
    add_shortcode('offset_sidebar','offset_sidebar_shortcode');
    add_shortcode('sidebar','offset_sidebar_shortcode');
    function offset_sidebar_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "bgcolor" => "#f5f5f5",
            "position" => "side",
            "class" => ""
            ), $atts));

        $positionClass = ($position=="side") ? "offset-sidebar" : "no-offset-sidebar";

        return "<div class=\"" . $positionClass . " " . $class . "\" style=\"background-color:" . $bgcolor . ";\">" .
               do_shortcode($content).
               "</div>";
    }
}

if ( ! function_exists('youtube_shortcode') ) {
    add_shortcode('youtube','youtube_shortcode');
    function youtube_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "id" => "0",
            "autoplay" => "0",
            "controls" => "1",
            "end" => "",
            "list" => "",
            "listType" => "",
            "playsinline" => "0",
            "rel" => "0",
            "showinfo" => "1",
            "start" => "",
            "size" => "medium"
        ), $atts));

        if($atts['id'] == "" || $id == "0"){
            return "<div class=\"shortcode-error\">INVALID YOUTUBE SHORTCODE</div>";
        }
        else{
            return "<div class=\"video-player ".$size."\"><div class=\"video-container\"><div class=\"ytplayer\" id=\"ytplayer-" . $id . "\" name=\"" . $id .
                "\" data-autoplay=\"" . $autoplay . "\"" .
                "\" data-controls=\"" . $controls . "\"" .
                "\" data-end=\"" . $end . "\"" .
                "\" data-list=\"" . $list . "\"" .
                "\" data-listType=\"" . $listType . "\"" .
                "\" data-playsinline=\"" . $playsinline . "\"" .
                "\" data-rel=\"" . $rel . "\"" .
                "\" data-showinfo=\"" . $showinfo . "\"" .
                "\" data-start=\"" . $start . "\"" .
                "></div></div><small>" . $content . "</small></div>";
        }
    }
}

if ( ! function_exists('vimeo_shortcode') ) {
    add_shortcode('vimeo', 'vimeo_shortcode');
    function vimeo_shortcode($atts, $content = null) {
        extract(shortcode_atts(array(
            'id' => '',
            'width' => '400',
            'height' => '225',
            'title' => '1',
            'byline' => '1',
            'portrait' => '1',
            'color' => '',
            "position" => "",
            "size"  => "medium",
            "class" => '',
            "embed" => ''
        ), $atts));

        if (empty($id) || !is_numeric($id)) return "<div class=\"shortcode-error\">INVALID VIMEO SHORTCODE</div>";
        if ($height && !$atts['width']) $width = intval($height * 16 / 9);
        if (!$atts['height'] && $width) $height = intval($width * 9 / 16);
        if ($content != "") {
            $content = "<small>" . $content . "</small>";
        }
        if ($position != "") {
            $containerClass = ($position == "side") ? "video-player " . $size . " right-offset" : "video-player";
        } else {
            $containerClass = "video-player " . $size;
        }

        return
            "<div class=\"".$containerClass."\"><div class=\"".$class." video-container vimeo\">".
                "<iframe src='https://player.vimeo.com/video/$id?$embed&amp;title=$title&amp;byline=$byline&amp;portrait=$portrait&amp;color=$color' width='$width' height='$height' frameborder='0' webkitallowfullscreen mozallowfullscreen allowfullscreen allow='autoplay'></iframe>".
            "</div>" . $content . "</div>";
    }
}
if ( ! function_exists('soundcloud_shortcode') ) {
    add_shortcode('soundcloud','soundcloud_shortcode');
    function soundcloud_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "url" => "",
            "params" => "",
            "width" => "",
            "height" => "",
            "iframe" => ""
            ), $atts));
        return "<div class=\"soundcloud-wrapper\"><iframe width=\"" . $width . "\" height=\"" . $height . "\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=" . $url . "&amp;" . $params . "\"></iframe></div>";
    }
}

if ( ! function_exists('iframe_shortcode') ) {
    add_shortcode('iframe', 'iframe_shortcode');
    function iframe_shortcode($atts, $content = null) {
        extract(shortcode_atts(array(
            'url'   => '',
            'width' => '',
            'height'=> '',
            'class' => '',
            'id'    => ''
            ), $atts));
        return "<iframe class=\"" . $class . "\" id=\"" . $id . "\" width=\"" . $width . "\" height=\"" . $height . "\" scrolling=\"no\" frameborder=\"no\" src=\"" . $url . "\"></iframe>";
    }
}


if ( ! function_exists('column_area_shortcode') ) {
    add_shortcode('column_area', 'column_area_shortcode');
    function column_area_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "shaded" => "",
            "title" => "",
            "number" => "2"
            ), $atts));
        $title = ($title == "") ? $title : "<h4>" . $title . "</h4>";
        return "<div class=\"col-nested col-" . $number . "-nested " . $shaded . "\">" . $title . do_shortcode($content) . "</div>";
    }
}

if ( ! function_exists('column_shortcode') ) {
    add_shortcode('column', 'column_shortcode');
    function column_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            ), $atts));
        return "<div class=\"nested-col\">" . do_shortcode($content) . "</div>";
    }
}

if ( ! function_exists('audio_shortcode') ) {
    add_shortcode('audio','audio_shortcode');
    function audio_shortcode($atts, $content = null)
    {
        //testing auto deploy
        extract(shortcode_atts(array(
            "mp3" => "",
            "title" => "",
            "position" => ""
        ), $atts));
        if($mp3 == "")
        {
            return "<div class=\"shortcode-error\">INVALID AUDIO SHORTCODE</div>";
        }
        else{
            $output = "<div id=\"jquery_jplayer_audio\" name=\"" . $mp3 . "\" class=\"jp-jplayer\"></div>" .
                "<div id=\"jp_container_audio\" class=\"jp-flat-audio\" role=\"application\" aria-label=\"media player\">" .
                "    <div class=\"jp-play-control jp-control\">" .
                "        <button class=\"jp-play jp-button\" role=\"button\" aria-label=\"play\" tabindex=\"0\"></button>" .
                "    </div>" .
                "    <div class=\"jp-bar\">" .
                "        <div class=\"jp-seek-bar jp-seek-bar-display\"></div>" .
                "        <div class=\"jp-seek-bar\">" .
                "	        <div class=\"jp-play-bar\"></div>" .
                "	        <div class=\"jp-details\"><span class=\"jp-title\" aria-label=\"title\">" . $title . "</span></div>" .
                "	        <div class=\"jp-timing\"><span class=\"jp-duration\" role=\"timer\" aria-label=\"duration\"></span></div>" .
                "        </div>" .
                "    </div>" .
                "    <div class=\"jp-no-solution\">" .
                "        Media Player Error<br />" .
                "        Update your browser or Flash plugin" .
                "    </div>" .
                "</div>" .
                "<div class=\"audio-caption\">" . $content . "</div>";
            if($position == "side")
            {
                $output = "<div class=\"audio-offset\">" . $output . "</div>";
            }
            return $output;
        }
    }
}

if ( ! function_exists('enews_widget_shortcode') ) {
    add_shortcode('enews_widget','enews_widget_shortcode');
    function enews_widget_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
        ), $atts));
        get_sidebar('enews');
    }
}
//Enable shortcodes in widgets
//add_filter('widget_text', 'do_shortcode');


//here1
function stop_paginate( $atts, $content = null ) {
   extract(shortcode_atts(array(
    //
    "max_items" => 2
     ), $atts));

    return $atts;

}




//function to remove empty paragraphs from short code
if( ! function_exists('shortcode_empty_paragraph_fix') ) {
    function shortcode_empty_paragraph_fix( $content ) {

        // define your shortcodes to filter, '' filters all shortcodes
        $shortcodes = array( 'enews_widget', 'audio','column','column_area','soundcloud','offset_sidebar','sidebar','full_width_video','full_width','article_list','widget_article_list','widget_single_image',
            'gallery','caption','iframe', 'youtube' );

        foreach ( $shortcodes as $shortcode ) {

            $array = array (
                '<p>[' . $shortcode => '[' .$shortcode,
                '<p>[/' . $shortcode => '[/' .$shortcode,
                $shortcode . ']</p>' => $shortcode . ']',
                $shortcode . ']<br />' => $shortcode . ']'
            );

            $content = strtr( $content, $array );
        }

        return $content;
    }
}

add_filter( 'the_content', 'shortcode_empty_paragraph_fix' );

if ( ! function_exists( 'simple_list_shortcode' ) ) {
    add_shortcode('simple_list', 'simple_list_shortcode');
    function simple_list_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "list_type" => null, //list type to pull,
            "sortorder"   => 'desc', //ASC or DESC
            "sortby"      => 'date',    //title or date
            "class"     => 'list-style',
            "show_images" => 'true',
            "show_title" => 'true'
        ), $atts));
        $simpleListItemPosts = get_posts(array(
            'posts_per_page' => -1,
            'post_type' => 'simple-list',
            'post_status' => 'publish',
            'tax_query' => array(
                array(
                    'taxonomy' => 'simple-list-types',
                    'field' => 'slug',
                    'terms' => $list_type
                )
            ),
            'order' => $sortorder,
            'orderby' => $sortby
        ));
        $simpleListItemsArray = array();
        foreach($simpleListItemPosts as $sli) {
            $listItem = new ArticleItem();
            if ( has_post_thumbnail($sli->ID) ) {
            $listItem->media = aab_get_image_tag(get_post_thumbnail_id($sli->ID), 'small');
            }
            else if (get_theme_mod('bp_article_list_default_image','')) {
                $listItem-> media = aab_get_image_tag(get_theme_mod('bp_article_list_default_image',''), 'small');
            }
            else {
                $listItem->media = "<img src=\"" .
                    get_template_directory_uri() . "/img/placeholder.png\" />";
            }
            $listItem->media_id = get_post_thumbnail_id($sli->ID);
            $listItem->media_size = 'thumbnail';
            $listItem->additional_markup = birdpress_get_the_content($sli->ID);
            array_push($simpleListItemsArray,$listItem);
        }


        if(sizeof($simpleListItemsArray) > 0) {
            return output_article_list(null,'simple-list ' . $class, null, $simpleListItemsArray, null, null, null, $show_images, $show_title);
        }
        else {
            return "<h3>No articles specified, either give a list of ids or a menu id</h3>";
        }
    }
}

if ( ! function_exists( 'accordion_shortcode' ) ) {
    add_shortcode('accordion', 'accordion_shortcode');
    function accordion_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "id" => '',
            "class" => '',
        ), $atts));
        return '<div class="accordion ' . $class . '" id="' . $id . '">' . do_shortcode($content) . '</div>';
    }
}

if ( ! function_exists( 'accordion_item_shortcode' ) ) {
    add_shortcode('accordion_item', 'accordion_item_shortcode');
    function accordion_item_shortcode($atts, $content = null)
    {
        extract(shortcode_atts(array(
            "id" => '',
            "parent_id" => '',
            "title" => '',
        ), $atts));
        return '<div class="card"><div class="card-header" id="heading' . $id . '"><h5 class="mb-0">' .
        '<div class="collapsed" type="button" data-toggle="collapse" data-target="#'. $id . '" aria-expanded="false" aria-controls="' . $id . '">' .
          $title . 
        '</div></h5></div>' .
        '<div id="'. $id . '" class="collapse" aria-labelledby="heading' . $id . '" data-parent="#' . $parent_id . '">' .
        '<div class="card-body">' . 
        $content . '</div></div></div>';
    }
}

if ( ! function_exists('hubspot_embed') ){
    add_shortcode('hubspot_embed', 'hubspot_embed_shortcode');
    function hubspot_embed_shortcode($atts, $content = null){
        extract(shortcode_atts(array(
            "form_id" => '',
            "target" => ''
        ), $atts));
        if($form_id == '' || $target == ''){
            return "<div class=\"shortcode-error\">INVALID HUBSPOT EMBED SHORTCODE, REQUIRES form_id and target</div>";
            //return $form_id . ' ' . $target;
        }
        else {
            return "<div class=\"" . $target . "\">
            <script type=\"text/javascript\" charset=\"utf-8\" src=\"//js.hsforms.net/forms/current.js\"></script>  
            <script type=\"text/javascript\">
              hbspt.forms.create({
                portalId: '95627',
                formId: '" . $form_id . "',
                target: '." . $target . "'
              });
            </script>
            </div>";
        }
    }
}


?>
