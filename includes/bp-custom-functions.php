<?php
/*
 *  Author: France Kehas-Dewaghe
 *  URL: allaboutbirds.org
 *  Misc functions used for birdpress
 */

/**
 * [add_slug_to_body_class  - Add page slug to body class, love this - Credit: Starkers Wordpress Theme]
 * @param [array of strings] $classes [classes assigned]
 */
if( ! function_exists('add_slug_to_body_class') ){
    function add_slug_to_body_class($classes)
    {
        global $post;
        if (is_home()) {
            $key = array_search('blog', $classes);
            if ($key > -1) {
                unset($classes[$key]);
            }
        } elseif (is_page()) {
            $classes[] = sanitize_html_class($post->post_name);
        } elseif (is_singular()) {
            $classes[] = sanitize_html_class($post->post_name);
        }

        return $classes;
    }
}

if (! function_exists('birdpress_show_lab_logo') ){
    function birdpress_show_lab_logo(){
        if(get_theme_mod('bp_show_lab_logo', 'yes') == 'yes'){
            return true;
        }
        else {
            return false;
        }
    }
}

if( ! function_exists('birdpress_sharing') ){
    /**
     * [birdpress_sharing - output markup for facebook, twitter, and pintrest sharing as well as link to comments section if present]
     * @return [string] [html markup]
     */
    function birdpress_sharing()
    {
        ?>
        <!-- Social Share Toolbox -->
                <?php if(
                    (is_single(get_the_ID()) && (get_theme_mod('bp_sharing_posts','on') == 'on' OR get_theme_mod('bp_sharing_posts','on') == '')
                    OR
                    (is_page(get_the_ID()) && get_theme_mod('bp_sharing_pages','on') == 'on' OR get_theme_mod('bp_sharing_pages','on') == '')
                    OR
                    (is_category() && get_theme_mod('bp_sharing_pages', 'on') == 'on' OR get_theme_mod('bp_sharing_pages', 'on') == '')
                    )) { ?>
                <div class="social-share">
                <div class="social-icons">
                <?php
                $url_link = '';
                if( is_category() ) {
                    $url_link = get_category_link( get_query_var('cat') );
                }
                else {
                    $url_link = get_permalink(get_the_ID());
                }
                $fb_link = "https://www.facebook.com/sharer.php?u=" . $url_link;
                $tw_link = "http://twitter.com/intent/tweet?url=" . $url_link;
                $pin_link = "https://pinterest.com/pin/create/button/?url=" . $url_link;
                ?>
                <a href="#" onClick="window.open('<?php echo $fb_link ?>','pagename','resizable,height=260,width=370'); return false;"><i id="facebook-share-article" class="fa fa-facebook-square facebook"></i></a>
                <a href="#" onClick="window.open('<?php echo $tw_link ?>','pagename','resizable,height=260,width=370'); return false;"><i id="twitter-share-article" class="fa fa-twitter-square twitter"></i></a>
                <a href="#" onClick="window.open('<?php echo $pin_link ?>','pagename','resizable,height=260,width=370'); return false;"><i id="pinterest-share-article" class="fa fa-pinterest-square pinterest"></i></a>
                </div>
                </div>
                <!-- Comment Counts -->
                <?php } if ( comments_open() && post_type_supports( get_post_type(), 'comments' ) && !is_category()) : ?>
                <div class="comment-box">
                    <a class="commentsAnchor"><i class="fa fa-comment"></i><span><?php echo get_comments_number(get_the_ID()); ?></span></a>
                </div>
                <?php endif; ?>
        <?php
    }
}

/**
 * [birdpress_pagination - ouput pagination markup]
 * @param  [int] $max_pages [max number of pages from the query]
 * @return [string]            [markup for pagination]
 */
if ( ! function_exists('birdpress_pagination') ){
    function birdpress_pagination($max_pages)
    {
        $retString = "";
        if ($max_pages > 1) {
            $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
            $queryParams = "";
            $queryParams = (get_query_var('s')) ? "s=" . get_query_var('s') : '';
            $sortParams = "";
            $sortParams = (get_query_var('orderby') AND isset($_GET['orderby'])) ? "&orderby=" . get_query_var('orderby') : '';
            $sortParams .= (get_query_var('order') AND isset($_GET['order'])) ? "&order=" . get_query_var('order') : '';
            if($queryParams != '') {
                $sortParams = '?' . $queryParams . $sortParams;
            }
            else if ($sortParams != ''){
                $sortParams = '?' . $sortParams;
            }
            $retString .= "<nav><ul class=\"pagination\">";
            if( (int) $paged > 1 ) {
                $retString .= "<li>" . get_previous_posts_link( '<<' ) . "</li>";
            }
            for ( $i = 1; $i <= $max_pages; $i++ ) {
                $pageLink = $url = strtok(get_pagenum_link($i), '?') . $sortParams;
                $retString .= "<li class=\"" . (($i == (int)$paged) ? "active" : "") . "\"><a href=\"" . $pageLink . "\" aria-label=\"Previous\">" . $i . "</a></li>";
            }
            if ( (int) $paged < $max_pages) {
                $retString .= "<li>" . get_next_posts_link( '>>', $max_pages ) . "</li>";
            }
            $retString .= "</ul></nav>";
        }
        return $retString;
    }
}

// Custom Comments Callback
function birdpresscomments($comment, $args, $depth)
{
	$GLOBALS['comment'] = $comment;
	extract($args, EXTR_SKIP);

	if ( 'div' == $args['style'] ) {
		$tag = 'div';
		$add_below = 'comment';
	} else {
		$tag = 'li';
		$add_below = 'div-comment';
	}
?>
    <!-- heads up: starting < for the html tag (li or div) in the next line: -->
    <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) : ?>
	<div id="div-comment-<?php comment_ID() ?>" class="comment-body">
	<?php endif; ?>
	<div class="comment-author vcard">
	<?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
	<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
	</div>
<?php if ($comment->comment_approved == '0') : ?>
	<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
	<br />
<?php endif; ?>

	<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
		<?php
			printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
		?>
	</div>

	<?php comment_text() ?>

	<div class="reply">
	<?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
	</div>
	<?php if ( 'div' != $args['style'] ) : ?>
	</div>
	<?php endif; ?>
<?php }

if ( ! function_exists( 'birdpress_output_sortbox' ) ){
    /**
     * [birdpress_output_sortbox - output sortbox markup used for article lists and set initial values]
     * @param  [string] $menu_class [class/s to assign to the ul of the sortbox]
     * @return [string]             [outputs html markup for a sort box of order and order by]
     */
    function birdpress_output_sortbox($menu_class){
        $order_by_sort = (get_query_var('orderby')) ? get_query_var('orderby') : 'date';
        $order_sort = (get_query_var('order')) ? get_query_var('order') : 'DESC';
        $search_string = (get_query_var('s')) ? '&s=' . get_query_var('s') : '';

        $retSortString = "";
        $retSortString .= "<div>" .
                    "<div class=\"sortby-dropdown\">" .
                      "<button class=\"btn btn-default dropdown-toggle\" type=\"button\" id=\"dropdownMenu1\" data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"true\">" .
                        "Sort By" .
                        "<span class=\"caret\"></span>" .
                      "</button>" .
                      "<ul class=\"" . $menu_class . "\" aria-labelledby=\"dropdownMenu1\">" .
                        "<li><a href=\"?orderby=date&order=DESC" . $search_string . "\">";
        if ($order_sort == 'DESC' and $order_by_sort == 'date') {
            $retSortString .= "<span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span>";
        }
        $retSortString .= "Date - Newest First</a></li>" .
                "<li><a href=\"?orderby=date&order=ASC" . $search_string . "\">";
        if ($order_sort == 'ASC' and $order_by_sort == 'date') {
            $retSortString .= "<span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span>";
        }
        $retSortString .= "Date - Oldest First</a></li>" .
                "<li role=\"separator\" class=\"divider\"></li>" .
                "<li><a href=\"?orderby=title&order=ASC" . $search_string . "\">";
        if ($order_sort == 'ASC' and $order_by_sort == 'title') {
            $retSortString .= "<span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span>";
        }
        $retSortString .= "Title A-Z</a></li>" .
                "<li><a href=\"?orderby=title&order=DESC" . $search_string . "\">";
        if ($order_sort == 'DESC' and $order_by_sort == 'title') {
            $retSortString .= "<span class=\"glyphicon glyphicon-ok\" aria-hidden=\"true\"></span>";
        }
        $retSortString .= "Title Z-A</a></li></ul></div></div>";
        return $retSortString;
    }
}

if ( ! function_exists('output_fullbleed_banner') ) {
    /**
     * [output_fullbleed_banner - output html markup for fullbleed banners (youtube, image, video)]
     * @param  [string] $banner_image     [url of image (jpg or png)]
     * @param  [string] $banner_video     [url of video (mp4)]
     * @param  [string] $banner_youtube   [id of youtube]
     * @param  [string] $banner_textstyle [class/s to assign to top div after full-bleed]
     * @param  [string] $custom_byline    [text overlay for banner]
     * @param  [string] $the_date         [Date to display]
     * @param  [string] $the_title        [Title to display]
     * @param  [string] $the_caption      [Caption of banner image/video]
     * @param  [boolean] $showcaret       [whether or not to show jump down caret]
     * @return [string]                   [html markup of fullbleed banner]
     */
    function output_fullbleed_banner($banner_image, $banner_video, $banner_youtube, $banner_textstyle, $custom_byline, $the_date, $the_title, $the_caption, $showcaret)
    { 
        $retString = '<div class="full-bleed ' . $banner_textstyle . '" style="height: 960.188px;">';
        $retString .= '<div class="section-overlay"></div>';
        if( ! empty($banner_youtube) ) {
            $retString .= '<div class="video-banner-wrapper youtube-full-feature" style="display:none">' .
                '<div id="ytplayer_' . $banner_youtube . '" name="' . $banner_youtube .
                '" class="ytplayer video-container" data-autoplay="0" data-controls="1" data-playsinline="0" data-rel="0" data-showinfo="1"></div></div>';
        }
        $retString .= '<div class="article-banner-wrapper img-full-feature" style="background-image: url(' . $banner_image . ')">';
        if ( ! empty($banner_video) ) {
            $retString = $retString . '<video autoplay loop muted class="ff-video banner-video-full-feature">' .
                '<source src="' . $banner_video . '" type="video/mp4">' .
	            '</video>';
        }
        $retString = $retString . '</div>' .
            '<div class="text-overlay-wrapper">' .
            '    <div class="text-overlay-middle">';
        if (! empty($the_caption)) {
            $retString .= "<div class='caption'>" . html_entity_decode($the_caption) . "</div>";
        }
        if( !empty($the_title) OR !empty($custom_byline) OR !empty($the_date) ) {
            if( ! empty($banner_video) ) {
                $retString .= '    <div class="text-article-display video">';
            }
            else {
                $retString .= '    <div class="text-article-display">';
            }
            if ( ! empty($the_title) ) {
                $retString .= '       <h1 class="bannerTitle">' . $the_title . '</h1>';
            }
            if ( ! empty($custom_byline) ) {
                $retString .= '       <div class="text-overlay">' . $custom_byline . '</div>';
            }
            if (! empty($the_date) ) {
                $retString .=  '       <div class="bannerDate">' . $the_date . '</div>';
            }
            if ( ! empty($banner_youtube) ) {
                $retString = $retString . '     <a href="javascript:void(0)" name="' . $banner_youtube . '" class="banner_video_button play-banner"><span class="fa fa-play-circle"></span></a>';
            }
            $retString .= '    </div>';
        }
        if ( ! empty($banner_video) ) {
            if($showcaret) {
                $retString = $retString . '<div class="bannerScroll"><i class="fa fa-chevron-down"></i></div>';
            }
            $retString = $retString .
            '       <div class="mute-button-banner hidden-xs">Listen</div>';
        }

        $retString = $retString . '    </div></div></div>';
        return $retString;
    }
}

if( ! function_exists('birdpress_blank_view_article') ) {
    function birdpress_blank_view_article($post_id)
    {
        if( get_post_meta($post_id, 'original_guid', true) ) {
            return ' <a class="view-article" href="' . get_post_meta($post_id, 'original_guid', true) . '">' . __('See More', 'birdpress') . '</a>';
        }
        else {
            return ' <a class="view-article" href="' . get_permalink($post_id) . '">' . __('See More', 'birdpress') . '</a>';
        }
    }
}

if (! function_exists('birdpress_get_sizeof_excerpt') ){
    function birdpress_get_sizeof_excerpt(){
        if(get_theme_mod('bp_article_list_excerpt_length',55)) {
            return get_theme_mod('bp_article_list_excerpt_length',55);
        }
        else {
            return 55;
        };
    }
}
if (! function_exists('birdpress_get_the_excerpt') ) {
    /**
     * [birdpress_get_the_excerpt - get a post's excerpt by id]
     * @param  [int] $post_id [post id]
     * @return [string]       [excerpt passed through standard wordpress filters]
     */
    function birdpress_get_the_excerpt($post_id) {
        $excerpt = apply_filters('the_excerpt', get_post_field('post_excerpt', $post_id));
        if($excerpt == ''){
            $excerpt = strip_shortcodes(wp_strip_all_tags(get_post_field('post_content', $post_id)));
            if($excerpt != ''){
                $excerpt = explode(' ', $excerpt, birdpress_get_sizeof_excerpt() + 1);
                if (count($excerpt)>=birdpress_get_sizeof_excerpt() + 1) {
                    array_pop($excerpt);
                    if($excerpt != '') {
                        $excerpt = implode(" ",$excerpt) . '...' . birdpress_blank_view_article($post_id);
                    }
                    else{
                        $excerpt = implode(" ", $excerpt);
                    }
                } else {
                    if($excerpt != '') {
                        $excerpt = implode(" ",$excerpt) . '...' . birdpress_blank_view_article($post_id);
                    }
                    else{
                        $excerpt = implode(" ", $excerpt);
                    }
                }
            }
        }
        $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
        return $excerpt;
    }
}

function delete_all_between($beginning, $end, $string) {
  $beginningPos = strpos($string, $beginning);
  $endPos = strpos($string, $end);
  if ($beginningPos === false || $endPos === false) {
    return $string;
  }

  $textToDelete = substr($string, $beginningPos, ($endPos + strlen($end)) - $beginningPos);

  return str_replace($textToDelete, '', $string);
}

if( ! function_exists('birdpress_get_the_content') ) {
    /**
     * [birdpress_get_the_content - get a post's content by id]
     * @param  [int] $post_id    [post id]
     * @return [string]          [post's content passed through 'the_content' filters]
     */
    function birdpress_get_the_content($post_id) {
        $t_post = get_post($post_id);
        return apply_filters('the_content', $t_post->post_content);
    }
}

if ( ! function_exists('aab_get_image_tag') ) {
    /**
     * [aab_get_image_tag - take image id and size and output html5 responsive <img> tag with alt]
     * @param  [int] $img_id [id of image in media library]
     * @param  [string] $size   [wordpress media size]
     * @return [string]         [html img tag]
     */
    function aab_get_image_tag($img_id, $size) {
        $img_src = wp_get_attachment_image_url( $img_id, $size );
        $img_srcset = wp_get_attachment_image_srcset( $img_id );
        $attr = wp_prepare_attachment_for_js($img_id);
        $img_sizes = wp_calculate_image_sizes($size, $img_src,null,$img_id);
        if( get_post_meta($img_id, 'bp_media_animated', true) ) {
            $img_src = wp_get_attachment_image_url($img_id, 'full');
            return "<img src='". esc_url( $img_src ) . "' alt='" . $attr['alt'] . "'>";
        }
        else {
            return "<img src='". esc_url( $img_src ) . "' srcset='" . esc_attr( $img_srcset ) . "' sizes='" . esc_attr($img_sizes) . "' alt='" . $attr['alt'] . "'>";
        }
    }
}

if ( ! function_exists('custom_widget_title') ) {
    /**
     * [custom_widget_title - wrap widget titles in <h3>]
     * @param  [string] $title [widget's title]
     * @return [string]        [widget's title wrapped in <h3>]
     */
    function custom_widget_title($title) {
        $title = ($title != "") ? "<h3>" . $title . "</h3>" : "";
        return $title;
    }
    //tie filter to function
    add_filter('widget_title', 'custom_widget_title');
}

if ( ! function_exists('detect_mobile_browser') ) {
    /**
     * [detect_mobile_browser - detect if user is on a mobile browser based on the useragent]
     * @return [bool] [true if a mobile browser useragent]
     */
    function detect_mobile_browser() {
        $useragent=$_SERVER['HTTP_USER_AGENT'];
        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4))) {
            return true;
        }
        else {
            return false;
        }
    }
}

if( ! function_exists('birdpress_extraHeaderNav') ) {
    function birdpress_extraHeaderNav() {
    }
}

function bp_media_animated( $form_fields, $post ) {
    $form_fields['bp-media-animated'] = array(
        'label' => 'Is Animated Gif?',
        'input' => 'html',
        'html' => '<label for="attachments-'.$post->ID.'-bp-media-animated"> '.
        '<input type="checkbox" id="attachments-'.$post->ID.'-bp-media-animated" name="attachments['.$post->ID.'][bp-media-animated]" value="1"'.(get_post_meta($post->ID, 'bp_media_animated', true) ? ' checked="checked"' : '').' /></label>  ',
        'value' => get_post_meta($post->ID, 'bp_media_animated', true),
        'helps' => 'Check for yes'
    );

    return $form_fields;
}

add_filter( 'attachment_fields_to_edit', 'bp_media_animated', 10, 2 );

function bp_media_animated_save( $post, $attachment ) {
    if( isset( $attachment['bp-media-animated'] ) )
        update_post_meta( $post['ID'], 'bp_media_animated', true );
    else
        update_post_meta( $post['ID'], 'bp_media_animated', false);

    return $post;
}

add_filter( 'attachment_fields_to_save', 'bp_media_animated_save', 10, 2 );

if ( ! function_exists('bp_header_misc_menu') ) {
    function bp_header_misc_menu($class){
        ?>
        <div class="<?php echo $class ?>">
            <ul class="nav navbar-nav top-one nav-pill">
                <?php birdpress_extraHeaderNav(); ?>
                <?php if(get_theme_mod('bp_enews_link','')) { ?>
                <li class="nav-enews"><a href="<?php echo get_theme_mod('bp_enews_link','') ?>">Get eNews</a></li>
                <?php } ?>
                <?php if(get_theme_mod('bp_contact_us_link','')) { ?>
                <li class="nav-contact"><a href="<?php echo get_theme_mod('bp_contact_us_link','') ?>">Contact Us</a></li>
                <?php } ?>
                <?php if(get_theme_mod('bp_show_search','yes') == 'yes') {?>
                <li class="nav-search"><a href="#" data-toggle="modal" data-target=".search-modal">Search</a></li>
                <?php } ?>
                <?php if(get_theme_mod('bp_donate_link','')) { ?>
                <li class="nav-donate"><a href="<?php echo get_theme_mod('bp_donate_link','') ?>">Donate</a></li>
                <?php } ?>
            </ul>
        </div>
        <?php
    }
}

if (! function_exists('birdpress_search_results_text') ) {
    function birdpress_search_results_text(){
        return 'search result(s) for';
    }
}

if( ! function_exists('birdpress_search_button_text') ) {
    function birdpress_search_button_text() {
        return 'Go';
    }
}

if( ! function_exists('birdpress_search_modal_text') ) {
    function birdpress_search_modal_text() {
        return 'Search';
    }
}

?>
