<?php
/*
 *  Author: France Kehas-Dewaghe
 *  URL: devbirdpress.org
 *  Functions and Classes for handling of article lists
 */

//Define Class
if ( ! class_exists('ArticleItem') ){
    class ArticleItem {
        public $media_type; //content format
        public $article_link; //link
        public $media; //image
        public $excerpt;
        public $external_link;
        public $article_title;
        public $article_date;
        public $additional_markup; //use to hold any additional markup
        public $categories;
        public $project;
    }
}

/*
    Output html of a list of posts and associated fields
*/
if ( ! function_exists('output_article_list') ) {
    /**
     * [output_article_list - take a list of ArticleItem and parameters and output html markup]
     * @param  [string] $article_list_id    [html id of wrapping div]
     * @param  [string] $article_list_class [html class of wrapping div]
     * @param  [string] $article_list_title [title to go in h3 of div]
     * @param  [object array of ArticleItem] $list_of_articles   [list of objects of type ArticleItem]
     * @param  [string] $show_excerpts      [true or false to show the excerpts of the articles]
     * @param  [string] $att_top_field      [the attribute of ArticleItem to show in the top span]
     * @param  [string] $att_bottom_field   [the attribute of ArticleItem to show in the bottom span]
     * @param   [string] $show_images 'true' or 'false'
     ** @return [string]                     [html markup]
     */
    function output_article_list($article_list_id, $article_list_class, $article_list_title, $list_of_articles, $show_excerpts, $att_top_field, $att_bottom_field, $show_images, $show_title, $carousel = false, $slick_class = '')
    {
        $retString = "";
        if(sizeof($list_of_articles) <= 0) { 
        $retString = "<div>Sorry, no articles found which match the criteria</div>";
        }
        else {
            if($show_images == 'false') {
                $article_list_class .= " no-image";
            }
            $retString .= "<div class=\"article-list " . $article_list_class . "\""; 
            if(isset($article_list_id)){ $retString .= " id=\"" . $article_list_id . "\""; }
            $retString .= ">";
            if(isset($article_list_title)) 
            { 
                $retString .= "<h3 class=\"articlelist-header\">" . $article_list_title . "</h3>";
            } 
            if((isset($list_of_articles) && (count($list_of_articles) > 0))) { 
                $retString .= "<ul>";
                foreach($list_of_articles as $item) {
                    $retString .= "<li class=\"article-item\">";   
                    if($show_images == 'true') {                 
                        $retString .= "<div class=\"article-item-media "; 
                        if(isset($item->media_type)){ $retString .= $item->media_type; } 
                        $retString .= "\">";
                        if( isset($item->article_link) OR isset($item->external_link) ){
                            $retString .= "<a href=\"" . (empty($item->external_link) ? $item->article_link : $item->external_link) . "\">" . $item->media . "<span class=\"media-icon\"></span></a>";
                        }
                        else{
                            $retString .= $item->media;
                        }
                        $retString .= "</div>";
                    }
                    if(isset($show_title) AND $show_title == 'true') {
                        if(isset($att_top_field) OR isset($item->article_link) OR isset($item->external_link) OR isset($item->excerpt) OR isset($att_bottom_field) OR isset($item->additional_markup)) {
                            $retString .= "<div class=\"article-item-body\">";
                            if(isset($item->additional_markup)) {
                                $retString .= "<div class=\"article-item-inner\">" . $item->additional_markup . "</div>";
                            }
                            if(isset($att_top_field)) { 
                                $retString .= "<span class=\"attribution\">" . $item->$att_top_field . "</span>";
                            } 
                            if ( isset($item->article_link) OR isset($item->external_link) ) {
                                $retString .= "<h4 class=\"article-item-header\">";
                                $retString .= "<a href=\"" . (empty($item->external_link) ? $item->article_link : $item->external_link ) . "\">" . $item->article_title . "</a>";
                                if(!empty($item->external_link) && $item->external_link) { 
                                    $retString .= "<i class=\"fa fa-external-link\"></i>";
                                } 
                                $retString .= "</h4>";
                            }
                            if($show_excerpts == 'true' && isset($item->excerpt)) { 
                                $retString .= "<p class=\"article-excerpt\">" . $item->excerpt . "</p>"; 
                            }
                            if(isset($att_bottom_field)) { 
                                $retString .= "<span class=\"attribution\">" . $item->$att_bottom_field . "</span>";
                            }
                            $retString .= "</div>";
                        }
                    }
                    $retString .= "</li>";
                }
                $retString .= "</ul>";
            }
            $retString .= "</div>";    
        }
        return $retString;
    }
}

/*
    Take a wordpress menu of articles and convert to an array of ArticleItems to use in output_article_list
*/
if ( ! function_exists('convert_wordpress_menu_to_article_list') ) {
    /**
     * [convert_wordpress_menu_to_article_list - takes a wordpress menu and pushes its items through build_ArticleItem and returns an array of the created objects]
     * @param  [int] $id_navmenu [id of wordpress menu]
     * @return [array of ArticleItem objexts]             
     */
    function convert_wordpress_menu_to_article_list($id_navmenu)
    {
            $menu = wp_get_nav_menu_object ($id_navmenu);
            $menu_items = wp_get_nav_menu_items($menu->term_id);
            $articleList = array();
            foreach($menu_items as $article){                                                          
                $listItem = build_ArticleItem($article->object_id);
                array_push($articleList, $listItem);            
            }
            return $articleList;
    }
}

if ( ! function_exists('build_ArticleItem') ){
    /**
     * [build_ArticleItem take a post id and convert its parts into an ArticleItem object]
     * @param  [int] $id [id of a wordpress post]
     * @return [ArticleItem object]     
     */
    function build_ArticleItem($id){
        $listItem = new ArticleItem();
        $listItem->media_type = get_content_format_class(wp_get_post_terms( $id, 'content-format',array("fields" => "names") ) );
        $listItem->media_type_display = implode(', ', wp_get_post_terms( $id, 'content-format',array("fields" => "names") ));
        $listItem->article_link = get_permalink( $id );
        if ( has_post_thumbnail($id) ) {
        $listItem->media = aab_get_image_tag(get_post_thumbnail_id($id), 'thumbnail');    
        }
        else if (get_theme_mod('bp_article_list_default_image','')) {
            $listItem-> media = aab_get_image_tag(get_theme_mod('bp_article_list_default_image',''), 'thumbnail');
        }
        else {
            $listItem->media = "<img src=\"" . 
                get_template_directory_uri() . "/img/placeholder.png\" />";
        }      
        $listItem->media_id = get_post_thumbnail_id($id);
        $listItem->media_size = 'thumbnail';
        $listItem->article_title = get_the_title($id);
        $listItem->article_date = get_the_date( 'F j, Y', $id );
        $listItem->excerpt = birdpress_get_the_excerpt($id);
        $listItem->external_link = get_post_meta($id, 'original_guid', true);
        $output = array_map(function ($object) { return $object->name; }, get_the_category($id));
        $listItem->categories = implode(', ', $output);
        if(taxonomy_exists('project')){
            if(sizeof(wp_get_post_terms( $id, 'project',array("fields" => "names"))) > 0){
                $listItem->project = wp_get_post_terms( $id, 'project',array("fields" => "names"))[0];
            }
            else{
                $listItem->project = '';
            }
        }
        return $listItem;
    }
}
?>