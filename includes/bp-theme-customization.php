<?php
function birdpress_customize_register( $wp_customize ) {
    $wp_customize->add_section( 'bp_custom_footer' , array(
        'title'      => __( 'Footer', 'birdpress' ),
        'priority'   => 30,
    ) );
    $wp_customize->add_setting( 'bp_twitter_link' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );
    $wp_customize->add_control(
	    'bp_twitter_link',
	    array(
		    'label'      => __( 'Twitter Link', 'birdpress' ),
		    'section'    => 'bp_custom_footer',
		    'settings'   => 'bp_twitter_link'
	    )
    );
    $wp_customize->add_setting( 'bp_facebook_link' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );
    $wp_customize->add_control(
	    'bp_facebook_link',
	    array(
		    'label'      => __( 'Facebook Link', 'birdpress' ),
		    'section'    => 'bp_custom_footer',
		    'settings'   => 'bp_facebook_link'
	    )
    );
    $wp_customize->add_setting( 'bp_instagram_link' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );
    $wp_customize->add_control(
        'bp_instagram_link',
        array(
            'label'      => __( 'Instagram Link', 'birdpress' ),
            'section'    => 'bp_custom_footer',
            'settings'   => 'bp_instagram_link'
        )
    );

    //adding option to show/hide cornell Logo
    $wp_customize->add_setting( 'bp_cornell_logo' , array(
        'default'     => 'yes',
        'transport'   => 'refresh',
    ) );
    $wp_customize->add_control(
  	    'bp_cornell_logo',
  	    array(
  		    'label'      => __( 'Show Cornell Logo', 'birdpress' ),
  		    'section'    => 'bp_custom_footer',
  		    'settings'   => 'bp_cornell_logo',
          'type'      => 'select',
           'choices'   => array(
               'yes'  => __('yes'),
               'no' => __('no')
           )
  	    )
      );

    $wp_customize->add_section( 'bp_custom_header' , array(
        'title'      => __( 'Header / Partner Logo', 'birdpress' ),
        'priority'   => 20,
    ) );

    //Add Custom Partner Logo to Theme customizer menu
    $wp_customize->add_setting( 'bp_partner_logo_image' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );
    $wp_customize->add_control(
        new WP_Customize_Media_Control(
        $wp_customize,
        'bp_partner_logo_image',
        array(
            'label'      => __( 'Default Featured Image (optional)', 'birdpress' ),
            'section'    => 'bp_custom_header',
            'settings'   => 'bp_partner_logo_image'
        ) )
    );
    //end Custom Partner Logo

    $wp_customize->add_section( 'bp_page_width' , array(
        'title'      => __( 'Page Width', 'birdpress' ),
        'priority'   => 40,
    ) );

    //


    //Add Custom Partner Link to the Theme customizer menu
    $wp_customize->add_setting( 'bp_partner_logo_link' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );
    $wp_customize->add_control(
	    'bp_partner_logo_link',
	    array(
		    'label'      => __( 'Partner Link', 'birdpress' ),
		    'section'    => 'bp_custom_header',
		    'settings'   => 'bp_partner_logo_link'
	    )
    );
    //end Custom Partner Link

    //Add Custom Contact Us Link to Theme customizer menu
    $wp_customize->add_setting( 'bp_contact_us_link' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );
    $wp_customize->add_control(
	    'bp_contact_us_link',
	    array(
		    'label'      => __( 'Contact Us Link', 'birdpress' ),
		    'section'    => 'bp_custom_header',
		    'settings'   => 'bp_contact_us_link'
	    )
    );
    //end Custom contact us Link

    //Add Custom eNews Link to Theme customizer menu
    $wp_customize->add_setting( 'bp_enews_link' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );
    $wp_customize->add_control(
	    'bp_enews_link',
	    array(
		    'label'      => __( 'eNews Link', 'birdpress' ),
		    'section'    => 'bp_custom_header',
		    'settings'   => 'bp_enews_link'
	    )
    );
    //end Custom eNews Link


    //Add Custom Donate Link to Theme customizer menu
    $wp_customize->add_setting( 'bp_donate_link' , array(
        'default'     => '',
        'transport'   => 'refresh',
    ) );
    $wp_customize->add_control(
	    'bp_donate_link',
	    array(
		    'label'      => __( 'Donate Link', 'birdpress' ),
		    'section'    => 'bp_custom_header',
		    'settings'   => 'bp_donate_link'
	    )
    );
    //end Custom Donate Link

    $wp_customize->add_setting( 'bp_header_layout', array(
        'default'   => '',
        'transport' => 'refresh'
        ));
    $wp_customize->add_control(
        'bp_header_layout',
        array(
            'label'     => __( 'Header Layout: Default/Alternate', 'birdpress' ),
            'section'   => 'bp_custom_header',
            'settings'  => 'bp_header_layout',
            'type'      => 'select',
            'choices'   => array(
                ''  => __('Default'),
                'alt' => __('Alternate')
            )
        )
    );

    $wp_customize->add_setting( 'bp_header_style', array(
        'default' => 'clean',
        'transport' => 'refresh'
        ));
    $wp_customize->add_control(
        'bp_header_style',
        array(
            'label'     => __( 'Header Style: Default/Pill/Line', 'birdpress' ),
            'section'   => 'bp_custom_header',
            'settings'  =>'bp_header_style',
            'type'      => 'select',
            'choices'   => array(
                'clean'  => __('Default Style'),
                ''      =>  __('Pill Style'),
                'line'      =>  __('Line Style')
            )
        )
    );

    //Add custom option to not show lab logo
    $wp_customize->add_setting( 'bp_show_lab_logo', array(
        'default' => 'yes',
        'transport' => 'refresh'
        ));
    $wp_customize->add_control(
        'bp_show_lab_logo',
        array(
            'label'     => __( 'Show Lab Logo', 'birdpress' ),
            'section'   => 'bp_custom_header',
            'settings'  =>'bp_show_lab_logo',
            'type'      => 'select',
            'choices'   => array(
                'yes'  => __('yes'),
                'no'      =>  __('no')
            )
        )
    );
    //end custom show lab logo

    //Add custom option to not show search in header
    $wp_customize->add_setting('bp_show_search', array(
        'default'=>'yes','transport'=>'refresh'));
    $wp_customize->add_control(
        'bp_show_search',
        array(
            'label' => __('Show Search', 'birdpress'),
            'section' => 'bp_custom_header',
            'settings' => 'bp_show_search',
            'type' => 'select',
            'choices' => array(
                'yes' => __('yes'),
                'no' => __('no')
            )
        )
    );
    //end custom show search

    //Add custom theme color to Theme customizer menu
    $wp_customize->add_setting( 'bp_color_scheme', array(
        'default'   =>  'theme-ORANGE',
        'transport' =>  'refresh'
        ) );
    $wp_customize->add_setting( 'bp_unique_class', array(
        'default'   => '',
        'transport' => 'refresh'
        ) );
    $wp_customize->add_section( 'bp_color_scheme', array(
        'title'     => __( 'Color Scheme', 'birdpress' ),
        'priority'  => 70,
        ) );
    $wp_customize->add_control(
        new WP_Customize_Control(
        $wp_customize,
        'bp_color_scheme',
        array(
        'label'      => __( 'Color Scheme', 'birdpress' ),
		    'section'    => 'bp_color_scheme',
		    'settings'   => 'bp_color_scheme',
            'type'       => 'select',
            'choices'        => array(
                'theme-ORANGE'   => __( 'Orange' ),
                'theme-BLUE'  => __( 'Blue' ),
                'theme-GREEN' => __('Green')
                )
            ) )
        );

    $wp_customize->add_section( 'bp_page_width', array(
        'title'     => __( 'Page Width', 'birdpress' ),
        'priority'  => 40,
        ) );

        //Add custom theme color to Theme customizer menu
        $wp_customize->add_setting( 'bp_page_width', array(
            'default'   =>  'page-narrow',
            'transport' =>  'refresh'
            ) );
        $wp_customize->add_control(
            new WP_Customize_Control(
            $wp_customize,
            'enews_signup_embed',
            array(
                'label'      => __( 'Page Width', 'birdpress' ),
    		    'section'    => 'bp_page_width',
    		    'settings'   => 'bp_page_width',
                'type'       => 'select',
                'choices'        => array(
                    'page-narrow'   => __( 'Narrow' ),
                    'page-wide' => __('Wide')
                    )
                ) )
            );

    $wp_customize->add_control(
        'bp_unique_class',
        array(
            'label'      => __( 'CSS Class to add to Body tag', 'birdpress' ),
            'section'    => 'bp_color_scheme',
            'settings'   => 'bp_unique_class'
            )
    );
    //Add custom article list to the Theme customizer menu
    $wp_customize->add_setting( 'bp_article_list_class', array(
        'default'   => 'list-style',
        'transport' => 'refresh'
        ) );
    $wp_customize->add_setting( 'bp_article_list_excerpt', array(
        'default'   => 'false',
        'transport' => 'refresh'
        ) );
    $wp_customize->add_setting( 'bp_article_list_excerpt_length', array(
        'default'   => 55,
        'transport' => 'refresh'
        ) );
    $wp_customize->add_setting( 'bp_article_list_top_attr', array(
        'default'   => '',
        'transport' => 'refresh'
        ) );
    $wp_customize->add_setting( 'bp_article_list_bot_attr', array(
        'default'   => '',
        'transport' => 'refresh'
        ) );
    $wp_customize->add_setting( 'bp_article_list_default_image', array(
        'default' => '',
        'transport' => 'refresh'
        ) );
    $wp_customize->add_section( 'bp_article_list', array(
        'title'     => __( 'Article List Pages', 'birdpress' ),
        'priority'  => 80,
        ) );
    $wp_customize->add_control(
        'bp_article_list_class',
        array(
            'label'      => __( 'List Class', 'birdpress' ),
		    'section'    => 'bp_article_list',
		    'settings'   => 'bp_article_list_class',
            'type'       => 'select',
            'choices'        => array(
                'list-style'   => __( 'List' ),
                'card-three card-display'  => __( 'Cards' )
                )
            )
    );
    $wp_customize->add_control(
        'bp_article_list_excerpt',
        array(
            'label'      => __( 'Show Excerpts', 'birdpress' ),
		    'section'    => 'bp_article_list',
		    'settings'   => 'bp_article_list_excerpt',
            'type'       => 'select',
            'choices'        => array(
                'false'   => __( 'Hide' ),
                'true'  => __( 'Show' )
                )
            )
    );
    $wp_customize->add_control(
        'bp_article_list_excerpt_length',
        array(
            'label'      => __( 'Excerpt Length (in words)', 'birdpress' ),
            'section'    => 'bp_article_list',
            'settings'   => 'bp_article_list_excerpt_length'
        )
    );
    $wp_customize->add_control(
        'bp_article_list_top_attr',
        array(
            'label'      => __( 'Top Attribute', 'birdpress' ),
		    'section'    => 'bp_article_list',
		    'settings'   => 'bp_article_list_top_attr',
            'type'       => 'select',
            'choices'        => array(
                ''              => __( 'None' ),
                'media_type_display'    => __( 'Content Format' ),
                'article_date'  => __( 'Date' ),
                'categories'    => __( 'Categories' )
                )
            )
    );
    $wp_customize->add_control(
        'bp_article_list_bot_attr',
        array(
            'label'      => __( 'Bottom Attribute', 'birdpress' ),
		    'section'    => 'bp_article_list',
		    'settings'   => 'bp_article_list_bot_attr',
            'type'       => 'select',
            'choices'        => array(
                ''              => __( 'None' ),
                'media_type_display'    => __( 'Content Format' ),
                'article_date'  => __( 'Date' ),
                'categories'    => __( 'Categories' )
                )
            )
    );
    $wp_customize->add_control(
        new WP_Customize_Media_Control(
        $wp_customize,
        'bp_article_list_default_image',
        array(
            'label'      => __( 'Default Featured Image (optional)', 'birdpress' ),
            'section'    => 'bp_article_list',
            'settings'   => 'bp_article_list_default_image'
        ) )
    );
    $wp_customize->add_section( 'bp_sharing', array(
        'title'     => __( 'Sharing', 'birdpress' ),
        'priority'  => 90,
        ) );
    $wp_customize->add_setting( 'bp_sharing_posts', array(
        'default'   => 'on',
        'transport' => 'refresh'
        ) );
    $wp_customize->add_setting( 'bp_sharing_pages', array(
        'default'   => 'on',
        'transport' => 'refresh'
        ) );
    $wp_customize->add_control(
        'bp_sharing_posts',
        array(
            'label'      => __( 'Share Posts', 'birdpress' ),
            'section'    => 'bp_sharing',
            'settings'   => 'bp_sharing_posts',
            'type'       => 'select',
            'choices'        => array(
                'on'   => __( 'On' ),
                'off'  => __( 'Off' )
                )
            )
    );
    $wp_customize->add_control(
        'bp_sharing_pages',
        array(
            'label'      => __( 'Share Pages', 'birdpress' ),
            'section'    => 'bp_sharing',
            'settings'   => 'bp_sharing_pages',
            'type'       => 'select',
            'choices'        => array(
                'on'   => __( 'On' ),
                'off'  => __( 'Off' )
                )
            )
    );
    $wp_customize->add_section( 'bp_banner_interval', array(
        'title'     => __( 'Rotating Banners', 'birdpress' ),
        'priority'  => 100,
        ) );
    $wp_customize->add_setting( 'bp_banner_interval', array(
        'default'   => '8000',
        'transport' => 'refresh'
        ) );
    $wp_customize->add_control(
        'bp_banner_interval',
        array(
            'label'      => __( 'Carousel Interval (miliseconds)', 'birdpress' ),
            'section'    => 'bp_banner_interval',
            'settings'   => 'bp_banner_interval'
            )
    );
    $wp_customize->remove_control('blogdescription');
}
add_action( 'customize_register', 'birdpress_customize_register');
?>
