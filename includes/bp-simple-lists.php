<?php
add_action('init', 'create_post_type_simple_list');
if(! function_exists('create_post_type_simple_list') ){
  function create_post_type_simple_list()
  {
      register_post_type('simple-list', // Register Custom Post Type
          array(
          'labels' => array(
              'name' => __('Simple List Items', 'birdpress'), // Rename these to suit
              'singular_name' => __('Simple List Item', 'birdpress'),
              'add_new' => __('Add New', 'birdpress'),
              'add_new_item' => __('Add New Simple List Item', 'birdpress'),
              'edit' => __('Edit', 'birdpress'),
              'edit_item' => __('Edit Simple List Item', 'birdpress'),
              'new_item' => __('New Simple List Item', 'birdpress'),
              'view' => __('View Simple List Item', 'birdpress'),
              'view_item' => __('View Simple List Item', 'birdpress'),
              'search_items' => __('Search Simple List Items', 'birdpress'),
              'not_found' => __('No Simple List Items found', 'birdpress'),
              'not_found_in_trash' => __('No Simple List Items
              found in Trash', 'birdpress')
          ),
          'taxonomies' => array('simple-list-types'),
          'public' => true,
          'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
          'has_archive' => true,
          'supports' => array(
              'title',
              'editor',
              'thumbnail'
          ),
          'show_in_nav_menus' => FALSE,
          'exclude_from_search' => true
      ));
  }
  add_action( 'init', 'create_simple_list_type_hierarchical_taxonomy', 0 );
}


if(! function_exists('create_simple_list_type_hierarchical_taxonomy') ) {
  function create_simple_list_type_hierarchical_taxonomy() {   
      //Type of article/Content Format
      $labels = array(
        'name' => _x( 'Simple List Types', 'taxonomy general name' ),
        'singular_name' => _x( 'Simple List Type', 'taxonomy singular name' ),
        'search_items' =>  __( 'Search Simple List Types'),
        'all_items' => __( 'All Search Simple List Types' ),
        'parent_item' => __( 'Parent Simple List Type' ),
        'parent_item_colon' => __( 'Simple List Type:' ),
        'edit_item' => __( 'Edit Simple List Type' ),
        'update_item' => __( 'Update Simple List Type' ),
        'add_new_item' => __( 'Add New Simple List Type' ),
        'new_item_name' => __( 'New Simple List Type' ),
        'menu_name' => __( 'Simple List Types' ),
      );
      register_taxonomy('simple-list-types',array('simple-list'), array(
        'hierarchical' => true,
        'labels' => $labels,
        'show_ui' => true,
        'show_admin_column' => true,
        'query_var' => true,
        'rewrite' => array( 'slug' => 'simple-list-type' ),
      ));
  }
}