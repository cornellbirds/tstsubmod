<?php
// Rotating Banners Post Type
add_action('init', 'create_post_type_rotating_banners'); // Add Birdpress Rotating Banners Post Type

function create_post_type_rotating_banners()
{
    register_post_type('rotating-banners', // Register Custom Post Type
        array(
        'labels' => array(
            'name' => __('Rotating Banners', 'birdpress'), // Rename these to suit
            'singular_name' => __('Rotating Banner', 'birdpress'),
            'add_new' => __('Add New', 'birdpress'),
            'add_new_item' => __('Add New Banner', 'birdpress'),
            'edit' => __('Edit', 'birdpress'),
            'edit_item' => __('Edit Banner', 'birdpress'),
            'new_item' => __('New Banner', 'birdpress'),
            'view' => __('View Banner', 'birdpress'),
            'view_item' => __('View Banner', 'birdpress'),
            'search_items' => __('Search Rotating Banners', 'birdpress'),
            'not_found' => __('No Banners found', 'birdpress'),
            'not_found_in_trash' => __('No Banners
            found in Trash', 'birdpress')
        ),
        'public' => true,
        'hierarchical' => true, // Allows your posts to behave like Hierarchy Pages
        'has_archive' => true,
        'supports' => array(
            'title',
            'editor'
        ),
        'show_in_nav_menus' => FALSE,
        'exclude_from_search' => true
    ));
}

// Output Rotating Banners
// --accepts a list of posts which should have banners attached
class Rotating_Banner_Slide {
    public $rbs_post_id;
    public $rbs_image_id;
    public $rbs_post_content;
}
function rotating_banners($post_type)
{
    $banner_posts = get_posts(array(        
        'posts_per_page' => -1,
        'post_type' => $post_type,
        'post_status' => 'publish'
    ));
    
    $banner_images = array();        
    
    foreach($banner_posts as $bp) {        
        $banner_post_images = get_posts(
                array(
                    'post_parent' => $bp->ID,
                    'post_type' => 'attachment',
                    'posts_per_page' => -1,
                    'post_status' => 'publish, inherit'
                 )
            );
        $new_slide = new Rotating_Banner_Slide();
        $new_slide->rbs_post_content = birdpress_get_the_content($bp->ID);
        $new_slide->rbs_post_id = $bp->ID;
        $new_slide->rbs_image_id = $banner_post_images[0]->ID;
        $new_slide->rbs_caption = get_posts(array('p' => $banner_post_images[0]->ID, 'post_type' => 'attachment'))[0]->post_excerpt;
        array_push($banner_images, $new_slide);
    }            
    
    if($banner_images != null) {
    ?>    
    <!-- Carousel -->
	<div  id="myCarousel" class="carousel slide home" data-ride="carousel" data-interval="<?php if(get_theme_mod('bp_banner_interval','8000')) { echo get_theme_mod('bp_banner_interval','8000'); } else { echo '8000'; } ?>">
	    <!-- Indicators -->
        <ol class="carousel-indicators">
    <?php    
        for ($x = 0; $x < sizeof($banner_images); $x++) {
        ?>
            <li data-target="#myCarousel" data-slide-to="<?php echo $x ?>" <?php if($x == 0) echo "class=\"active\"" ?>></li>
        <?php
        }
        ?>
        </ol>
		<!-- Wrapper for slides -->
		<div class="carousel-inner" role="listbox">
        <?php
        $counter = 0;
        foreach($banner_images as $bi) {
            $banner_image = aab_get_image_tag($bi->rbs_image_id,'full');
            $banner_text = $bi->rbs_post_content;
            $banner_caption = $bi->rbs_caption;
            ?>
            <div class="item <?php if($counter == 0) echo "active" ?>">
				<?php echo $banner_image ?>
                <div class="carousel-caption">
                <?php echo $banner_caption ?>				    
			    </div>
                <div class="carousel-text">
                <?php echo $banner_text ?>
                </div>
			</div>            
            <?php
            $counter += 1;
        }
        ?>
        </div>	
        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
	        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	        <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
	        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	        <span class="sr-only">Next</span>
        </a>
    </div>
    <?php    
    }
    else {
        echo "banner images null";
        echo sizeof($banner_posts);
        echo $banner_posts[0]->ID;
    }
}
?>