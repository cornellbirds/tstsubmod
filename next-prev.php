<?php
$next_article = get_post_meta( get_the_ID(), 'next_article', true );
$previous_article = get_post_meta( get_the_ID(), 'previous_article', true );
if($next_article || $previous_article)
{
?>
<section class="container">
  <div class="row">                                
    <div id="next-prev-links" class="col-lg-12 clearfix">
        <?php 
        if($previous_article)
        { 
        ?>
        <div class="prev"><a target="_self" href="<?php echo get_permalink($previous_article) ?>"><?php echo get_the_title($previous_article) ?></a></div>
        <?php 
        }
        if($next_article)
        {
        ?>
        <div class="next"><a target="_self" href="<?php echo get_permalink($next_article) ?>"><?php echo get_the_title($next_article) ?></a></div>
        <?php 
        }
        ?>                              
    </div>            
  </div>
</section>    
<?php } ?> 