<?php
    echo birdpress_output_sortbox('dropdown-menu dropdown-menu-right');   
    $allArticles = array();   
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
    $sort_order_by = (get_query_var('orderby')) ? get_query_var('orderby') : 'date';
    $sort_order = (get_query_var('order')) ? get_query_var('order') : 'DESC';
    $search_param = (get_query_var('s')) ? get_query_var('s') : '';
    if(get_query_var('bp-all-articles') == true) {        
        $args = array('order' => $sort_order, 'orderby' => $sort_order_by, 'paged' => $paged, 'numberposts' => get_option('posts_per_page'), 'offset' => (($paged - 1) * get_option('posts_per_page')),'post_type' => 'post', 'post_status' => 'publish', 's' => $search_param);
        $wp_query = new WP_Query( $args );
    }    
    else {       
        $args = array_merge( $wp_query->query_vars, array( 'order' => $sort_order, 'orderby' => $sort_order_by, 's' => $search_param ) );        
        $wp_query = new WP_Query( $args );
    }
    
    if (have_posts()):
        while (have_posts()) :
            the_post();
            $listItem = build_ArticleItem(get_the_ID());
            array_push($allArticles, $listItem);
        endwhile;
    endif;    
    echo output_article_list(null, get_theme_mod('bp_article_list_class','list-style'),null,$allArticles, 
        (get_theme_mod('bp_article_list_excerpt','false')=='')?null:get_theme_mod('bp_article_list_excerpt','false'),
        (get_theme_mod('bp_article_list_top_attr','')=='')?null:get_theme_mod('bp_article_list_top_attr',''), 
        (get_theme_mod('bp_article_list_bot_attr','')=='')?null:get_theme_mod('bp_article_list_bot_attr',''), 'true', 'true');
?>
