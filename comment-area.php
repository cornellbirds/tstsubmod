
<?php if ( comments_open() ) { ?>
<section id="comment-area" class="container">
    <div class="comment-sidebar-section">
    <?php
        get_sidebar();
    ?>
    </div>    
    <div class="comment-section">
    <h3>Comments</h3>
    <?php 
        comments_template(); 
    ?>
    </div>
</section> 
<?php } ?>
