<!doctype html>
<!--[if lt IE 7]>      <html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html <?php language_attributes(); ?> class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html <?php language_attributes(); ?> class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html <?php language_attributes(); ?> class="no-js"> <!--<![endif]-->
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>
        <meta name="description" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

		<link rel="shortcut icon" href="<?php echo get_site_icon_url(512,get_template_directory_uri() . '/img/icons/favicon.ico'); ?>" />
        <link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="description" content="<?php bloginfo('description'); ?>">

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(get_theme_mod('bp_color_scheme', 'theme-ORANGE') . ' ' . get_theme_mod('bp_unique_class', '') . ' ' . get_theme_mod('bp_page_width', '') ); ?>>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->


<div class="background-brand-bar">

	    <div class="brand-row">

            <div class="logos">
                  	<?php if(birdpress_show_lab_logo()) { ?>
				            <div class="logo-bg" role="logo" >
											<!--commit code-->
											<a href="http://www.birds.cornell.edu" target="_self" ></a>
					            <object type="image/svg+xml" data="<?php echo get_template_directory_uri() ?>/img/logo-clo--short.svg" class="svg-logo">The Cornell Lab of Ornithology</object>

				            </div>
		            <?php } ?>
                      <?php if(get_theme_mod('bp_partner_logo_image','') != '') { ?>
                      <div class="logo-second">
                          <a href="<?php echo get_theme_mod('bp_partner_logo_link',''); ?>" rel="parent" title="">
                              <?php echo aab_get_image_tag(get_theme_mod('bp_partner_logo_image',''),'full') ?>
                          </a>
                      </div>
                        <?php } ?>
            </div>

						<div class="top-one">

					        <! top-one menu hidden for mobile nav views ->
								  <?php bp_header_misc_menu('hidden-xs') ?>
						</div>

				</div>

	  </div>
		<header class="head
					<?php
					if(get_theme_mod('bp_header_layout','') != '')
					{
						echo ' ' . get_theme_mod('bp_header_layout','');
					}
					if(get_theme_mod('bp_partner_logo_image','') != '')
					{
						echo ' partner-logo';
					}
					if(get_theme_mod('bp_header_style',' clean'))
					{
						echo ' ' . get_theme_mod('bp_header_style', ' clean');
					}?>" role="banner">

    		<div class="site-name-row">

								<div class="btns-wrapper">

										<button type="button" class="btn-search-mobile visible-xs" data-toggle="modal" data-target=".search-modal" style="float: right">
														 <span class="fa fa-search"></span></span>
										</button>
										<button type="button" class="btn-nav-mobile navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
												<span class="sr-only">Toggle navigation</span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
												<span class="icon-bar"></span>
										</button>

								</div>


								<div id="website-name"><a href="<?php echo get_home_url() ?>" target="_self" ><?php bloginfo('name'); ?></a></div>


								<nav class="navbar navbar-default revised-navbar">
										<div id="navbar" class="navbar-collapse collapse right-side-nav container" aria-expanded="false" style="height: 1px;">

					                    <?php birdpress_nav('header-menu', 'top-two', 'global-nav'); ?>
					                    <! top-one menu nav added for mobile nav views ->
					                    <?php bp_header_misc_menu('row visible-xs') ?>

								    </div><!--/.nav-collapse -->
							</nav>
      </div>

</header>


<!-- modal search element-->
      <div class="modal fade search-modal" tabindex="-1" role="dialog" aria-labelledby="myMediumModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
            <h4 class="modal-title"><?php echo birdpress_search_modal_text(); ?></h4>
          </div>
          <div class="modal-body">
                  <div class="form-search clearfix">
                <input id="search-modal-text" type="text" class="span2 search-query pull-left form-control"/>
                <button type="submit" class="btn pull-left"><?php echo birdpress_search_button_text(); ?></button>
              </div>
          </div>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
  </div>
