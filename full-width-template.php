<?php
/**
 * Template Name: Birdpress Full Width Template
 *
 *
 */
get_header();
$banner_posts = get_posts(array(
        'posts_per_page' => -1,
        'post_type' => 'rotating-banners',
        'post_status' => 'publish'
    ));
if( sizeof($banner_posts) == 1 ) {
	$banner_post_images = get_posts(
        array(
            'post_parent' => $banner_posts[0]->ID,
            'post_type' => 'attachment',
            'posts_per_page' => -1,
            'post_status' => 'publish, inherit'
         )
    );
    if($banner_post_images) {
	   echo output_fullbleed_banner($banner_post_images[0]->guid, null, null, 'light narrower-height top-banner', apply_filters('the_content',$banner_posts[0]->post_content), null, null, get_posts(array('p' => $banner_post_images[0]->ID, 'post_type' => 'attachment'))[0]->post_excerpt, true);
    }
}
else if ($banner_posts AND sizeof($banner_posts) != 0){
	rotating_banners('rotating-banners');
}

?>
<div class="wrap content clearfix">
	<?php get_template_part('subnav'); ?>
    <section class="full-width" id="bp-article-content">
        <?php
        if (have_posts()):
            while (have_posts()) :
                the_post();
                    ?>
                    <!--<h1><?php the_title()?></h1>-->
                    <?php birdpress_sharing();
                    $displayFeaturedImage = get_post_meta( get_the_ID(), 'featured-image-display', true );
                    if ($displayFeaturedImage != "no"){ ?>
                    <?php if ( has_post_thumbnail() ) : ?>
                    <div class="photo medium">
                        <?php echo aab_get_image_tag( get_post_thumbnail_id( get_the_ID() ), 'medium' )  ?>
                        <small><?php echo get_posts(array('p' => get_post_thumbnail_id( get_the_ID() ), 'post_type' => 'attachment'))[0]->post_excerpt ?></small>
                    </div>
                    <?php endif;
                    }
                the_content();
            endwhile;
        endif;
        ?>

        <!--start content for CSF-->

        <div class="scrollToTop">
          <div class="scrollTop">
            <i class="glyphicon">&#xe113;</i>
            <p>scroll<br>to top</div></p>
          </div>
        </div>

        <article class="content">

          <h3>One of nature’s greatest wonders is at risk of vanishing.</h3>
          <p>Each year, millions of shorebirds migrate thousands of miles along the Pacific coast of the Americas, from their breeding grounds in the Arctic tundra of North America to their wintering grounds at the southernmost tip of Chile. These long-distance migrations evolved to depend on stopover and wintering sites - a network of coastal wetlands, estuaries, and beaches - known as the Pacific Americas Flyway.</p>
          <p>The habitats along the Pacific Americas Flyway also provide important ecosystem services to growing coastal communities. Many of these coastal ecosystems and the services they provide are threatened due to increasing pressures from expanding human development and climate change, which are contributing to on-going shorebird declines.</p>

        </article>

        <section class="google-mymap">
          <div class="map-overlay"></div>
          <div class='embed-container' id="map-canvas">
            <iframe  src="https://www.google.com/maps/d/embed?mid=16Ol1vW97L7WW2pK--1VwA-Rue8Y&amp;ll=36.67,  -121.77&z=3&t=h" ></iframe>
          </div>
            <article class="content"><p style="font-size:11px;">Senner, S. E., B. A. Andres and H. R. Gates (Eds.). 2016. Pacific Americas shorebird conservation strategy. National Audubon Society, New York, New York, USA. Available at: <a href="http://www.shorebirdplan.org">http://www.shorebirdplan.org</a>.</p></article>
        </section>

      <article class="content">

        <h3>New collaborations, new solutions</h3>

        <p>In order to target the complex challenges surrounding coastal development, we need new solutions that combine knowledge, expertise, and ideas from multiple disciplines and sectors. </p>
        <p>To address this need, the David and Lucile Packard Foundation and the Cornell Lab of Ornithology have partnered to create the Coastal Solutions Fellows Program. The Program is building a community of early-career leaders from the academic, private, and non-profit sectors that are working on new approaches to coastal development and ecosystem management.</p>
        <p>Want to learn more? Watch our video. </p>

      </article>

      <article class="content video-wrapper-csf">

          <div class='video-player-csf'>

            <iframe src="https://player.vimeo.com/video/241581619" width="1920" height="1080" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>

        </div>

      </article>

      </div>

      <article class="content">

        <h3>The Program</h3>

        <p>For the next decade, the Coastal Solutions Fellows Program will support early-career planners, developers and scientists from Latin America to collaboratively design and implement new solutions to tackle current challenges facing coastal ecosystems and communities.</p>

        <p>The program will support six young professionals per year to implement a project at a priority shorebird site in Latin America along the Pacific Americas Flyway. Fellows will be provided two years of funding, mentoring support, and professional development opportunities, including annual retreats that combine peer-to-peer learning and strategic trainings.</p>

      </article>

      <article id="signup" class="infographic signup" style="background-image:url('<?php echo home_url();?>/wp-content/uploads/2017/11/habitat-footer.jpg')">

        <div class="container">

          <div class="proposals center">


            <!-- Begin MailChimp Signup Form -->
            <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
            <style type="text/css">
            	#mc_embed_signup{clear:left; font:14px Helvetica,Arial,sans-serif; }
            	/* Add your own MailChimp form style overrides in your site stylesheet or in this style block.
            	   We recommend moving this block and the preceding CSS link to the HEAD of your HTML file. */

                 form input{
                   margin-bottom: 0;
                 }
                 form input[type="email"]{
                   float: none;
                 }
                 input[type="radio"],
                 input[type="checkbox"]{
                   margin: 4px 6px 4px 0;
                 }
            </style>
            <div id="mc_embed_signup">
            <form action="https://solucionescosteras.us2.list-manage.com/subscribe/post?u=b35ddb671faf4a16c0ce32406&amp;id=0112899e30" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">
            	<h4>Want to receive the latest news about the Fellowship?</h4>
            <div class="indicates-required"><span class="asterisk">*</span> indicates required</div>
            <div class="mc-field-group">
            	<label for="mce-EMAIL">Email Address <span class="asterisk">*</span></label>
            	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
            <div class="mc-field-group">
            	<label for="mce-FNAME">First Name </label>
            	<input type="text" value="" name="FNAME" class="" id="mce-FNAME">
            </div>
            <div class="mc-field-group">
            	<label for="mce-LNAME">Last Name </label>
            	<input type="text" value="" name="LNAME" class="" id="mce-LNAME">
            </div>
            <div class="mc-field-group">
            	<label for="mce-COUNTRY">Country </label>
            	<input type="text" value="" name="COUNTRY" class="" id="mce-COUNTRY">
            </div>
            <div class="mc-field-group input-group">
                <strong>Please check to receive more information </strong>
                <ul><li><input type="checkbox" value="1" name="group[5293][1]" id="mce-group[5293]-5293-0"><label for="mce-group[5293]-5293-0">For Applicants</label></li>
            <li><input type="checkbox" value="2" name="group[5293][2]" id="mce-group[5293]-5293-1"><label for="mce-group[5293]-5293-1">For Partners</label></li>
            </ul>
            </div>
            	<div id="mce-responses" class="clear">
            		<div class="response" id="mce-error-response" style="display:none"></div>
            		<div class="response" id="mce-success-response" style="display:none"></div>
            	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_b35ddb671faf4a16c0ce32406_0112899e30" tabindex="-1" value=""></div>
                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button" style="background-color:#00688E;"></div>
                </div>
            </form>
            </div>
            <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[4]='FNAME';ftypes[4]='text';fnames[1]='LNAME';ftypes[1]='text';fnames[3]='ORG';ftypes[3]='text';fnames[2]='COUNTRY';ftypes[2]='text'; /*
             * Translated default messages for the $ validation plugin.
             * Locale: ES
             */
            $.extend($.validator.messages, {
              required: "Este campo es obligatorio.",
              remote: "Por favor, rellena este campo.",
              email: "Por favor, escribe una dirección de correo válida",
              url: "Por favor, escribe una URL válida.",
              date: "Por favor, escribe una fecha válida.",
              dateISO: "Por favor, escribe una fecha (ISO) válida.",
              number: "Por favor, escribe un número entero válido.",
              digits: "Por favor, escribe sólo dígitos.",
              creditcard: "Por favor, escribe un número de tarjeta válido.",
              equalTo: "Por favor, escribe el mismo valor de nuevo.",
              accept: "Por favor, escribe un valor con una extensión aceptada.",
              maxlength: $.validator.format("Por favor, no escribas más de {0} caracteres."),
              minlength: $.validator.format("Por favor, no escribas menos de {0} caracteres."),
              rangelength: $.validator.format("Por favor, escribe un valor entre {0} y {1} caracteres."),
              range: $.validator.format("Por favor, escribe un valor entre {0} y {1}."),
              max: $.validator.format("Por favor, escribe un valor menor o igual a {0}."),
              min: $.validator.format("Por favor, escribe un valor mayor o igual a {0}.")
            });}(jQuery));var $mcj = jQuery.noConflict(true);</script>
            <!--End mc_embed_signup-->
          </div>

        </div>

        <!--<p class="photo-credit">Ecosistema Urbano</p>-->

      </article>


	</section>
</div>

<script type="text/javascript">

(function($) {
  $(document).ready(function(){
    $('a[href*="#"]')
      .not('[href="#"]')
      .not('[href="#0"]')
      .not('[href="#myCarousel"]')
      .click(function(event) {
        if (
          location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '')
          &&
          location.hostname == this.hostname
        ) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
          if (target.length) {
            event.preventDefault();
            $('html, body').animate({
              scrollTop: target.offset().top
            }, 1300, function() {
              var $target = $(target);
              $target.focus();
              if ($target.is(":focus")) {
                return false;
              } else {
                $target.attr('tabindex','-1');
                $target.focus();
              };
            });
          }
        }
      	}
      );

      $(window).scroll(function(){
    		if ($(this).scrollTop() > 400) {
    			$('.scrollToTop').fadeIn();
    		} else {
    			$('.scrollToTop').fadeOut();
    		}
    	});

    	$('.scrollToTop').click(function(){
    		$('html, body').animate({scrollTop : 0},800);
    		return false;
    	});

      //map overlay
      $('.map-overlay').click(function(){
        $(this).css('z-index','-1');
        $('iframe').css('z-index', '1');
      })

    });
  })( jQuery );

</script>

<?php get_template_part('next-prev'); ?>
<?php get_template_part('comment-area'); ?>
<?php get_footer(); ?>
