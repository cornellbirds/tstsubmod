<?php get_header(); ?>
<div class="wrap content col-2 clearfix">
	<article class="grid" role="article">
        <?php
        $category = get_category(get_query_var('cat'));
        $category_description = $category->category_description;
        $catTitle = $category->name;
        if(isset($category->category_parent) AND $category->category_parent != 0) {
            $category = get_category($category->category_parent);
            while ($category->category_parent != 0) {
                $catTitle = $category->name . " - " .  $catTitle;
                $category = get_category($category->category_parent);
            }
            $catTitle = $category->name . " - " .  $catTitle;
        }
        $has_children = get_categories( array( 'child_of' => $category->term_id ) );
        echo "<h1>" . $catTitle . "</h1>";
        birdpress_sharing();
        ?>
        <div class="category-page <?php if(!$has_children) { echo 'no-nav'; } ?>">
            <div class="category-description"><?php echo $category_description ?></div>
                <?php if ( $has_children ) { ?>
                <div class="nav-area">
                <?php if ( detect_mobile_browser() ) { ?>
                <button class="btn" type="button" data-toggle="collapse" data-target="#bp-subcategories" aria-expanded="false" aria-controls="bp-categories">Filter by <span class="fa fa-angle-down"></span></button>
                <?php }  ?>
                    <div class="collapse <?php if(!detect_mobile_browser()) { echo 'in'; } ?>" id="bp-subcategories">
                      <div class="well">
                        <?php wp_list_categories( array('title_li' => __('<a href="' . home_url() . '/category/'. $category->slug . '">' . $category->name . "</a>"), 'child_of' => $category->term_id, 'hide_empty' => '1' ) ); ?>
                      </div>
                    </div>
                </div>
                <?php } ?>
                <div class="cat-list-area">
                <?php
                   set_query_var('bp-list-type','list-style');
                   get_template_part('loop');
                   get_template_part('pagination', $wp_query->max_num_pages); ?>
                </div>
        </div>
	</article>
    <aside class="sidebar">
    <?php get_sidebar();?>
    </aside>
</div>
<?php get_footer(); ?>
