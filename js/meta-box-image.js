﻿/*
* Attaches the image uploader to the input field
*/
jQuery(document).ready(function ($) {
    //automatically check parent topic when child topic is checked
    jQuery("ul.categorychecklist li ul.children input").click(function () {
        var par = jQuery(this).closest("ul.children").closest("li").find("label input");
        jQuery(par[0]).prop("checked", true);
    });
    jQuery("ul.cat-checklist li ul.children input").click(function () {
        var par = jQuery(this).closest("ul.children").closest("li").find("label input");
        jQuery(par[0]).prop("checked", true);
    });
    // Instantiates the variable that holds the media library frame.
    var meta_image_frame;
    var meta_image_frame_two;

    // Runs when the image button is clicked.
    $('#banner-image-button').click(function (e) {

        // Prevents the default action from occuring.
        e.preventDefault();

        // If the frame already exists, re-open it..
        if (meta_image_frame) {
            meta_image_frame.open();
            return;
        }

        // Sets up the media library frame
        meta_image_frame = wp.media.frames.meta_image_frame = wp.media({
            title: meta_image.title,
            button: { text: meta_image.button }
        });

        // Runs when an image is selected.
        meta_image_frame.on('select', function () {

            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = meta_image_frame.state().get('selection').first().toJSON();

            // Sends the attachment URL to our custom image input field.
            $('#banner-image').val(media_attachment.url);
            $('#banner-image-preview').attr("src", media_attachment.url);
        });

        // Opens the media library frame.
        meta_image_frame.open();
    });
    $('#fallback-videobanner-image-button').click(function (e) {

        // Prevents the default action from occuring.
        e.preventDefault();

        // If the frame already exists, re-open it.
        if (meta_image_frame_two) {
            meta_image_frame_two.open();
            return;
        }

        // Sets up the media library frame
        meta_image_frame_two = wp.media.frames.meta_image_frame = wp.media({
            title: meta_image.title,
            button: { text: meta_image.button }
        });

        // Runs when an image is selected.
        meta_image_frame_two.on('select', function () {

            // Grabs the attachment selection and creates a JSON representation of the model.
            var media_attachment = meta_image_frame_two.state().get('selection').first().toJSON();

            // Sends the attachment URL to our custom image input field.
            $('#fallback-videobanner-image').val(media_attachment.url);
        });

        // Opens the media library frame.
        meta_image_frame_two.open();
    });
});