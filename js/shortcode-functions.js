﻿var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
function onYouTubeIframeAPIReady() {
    //code for youtube embeds            
    jQuery.each(jQuery(".ytplayer"), function (i, item) {
        player = new YT.Player(jQuery(item).attr('id'), {
            videoId: jQuery(item).attr('name'),
            playerVars: {
                'autoplay': jQuery(item).data('autoplay'),
                'controls': jQuery(item).data('controls'),
                'end': jQuery(item).data('end'),
                'list': jQuery(item).data('list'),
                'listType': jQuery(item).data('listType'),
                'playsinline': jQuery(item).data('playsinline'),
                'rel': jQuery(item).data('rel'),
                'showinfo': jQuery(item).data('showinfo'),
                'start': jQuery(item).data('start'),
                'modestbranding': 1,
                'wmode': 'opaque'
            },
            height: '360'
        });
    });
}
function resizeFullWidthImages() {
    var divWidth = jQuery(window).width();
    var divHeight16x9 = jQuery(window).width() * 0.5625;
    var divHeight235x1 = jQuery(window).width() / 2.35;

    jQuery.each(jQuery(".content-banner > .img-full-feature"), function (i, item) {
        var newMargin = -1 * (jQuery(window).width() - jQuery('#bp-article-content').width()) / 2;
        jQuery(item).children().css('height', divHeight16x9);
        jQuery(item).children().css('width', divWidth);
        jQuery(item).css('width', divWidth);
        jQuery(item).css('height', divHeight16x9);
        jQuery(item).parent().css('height', divHeight16x9);
        jQuery(item).css('margin-left', newMargin);
    });
    jQuery.each(jQuery(".top-banner > .img-full-feature"), function (i, item) {
        var newMargin2 = -1 * (jQuery(window).width() - jQuery('#bp-article-content').width()) / 2;
        jQuery(item).children().css('height', divHeight235x1);
        jQuery(item).children().css('width', divWidth);
        jQuery(item).css('width', divWidth);
        jQuery(item).css('height', divHeight235x1);
        jQuery(item).parent().css('height', divHeight235x1);
        if(jQuery('#bp-article-content').length){
            jQuery(item).css('margin-left', newMargin2);
        }
    });
    jQuery.each(jQuery(".top-banner > .youtube-full-feature"), function (i, item) {
        var newMargin2 = -1 * (jQuery(window).width() - jQuery('#bp-article-content').width()) / 2;
        jQuery(item).children().css('height', divHeight235x1);
        jQuery(item).children().css('width', divWidth);
        jQuery(item).css('width', divWidth);
        jQuery(item).css('height', divHeight235x1);
        jQuery(item).parent().css('height', divHeight235x1);
        jQuery(item).css('margin-left', newMargin2);
    });
    jQuery.each(jQuery(".banner-video-full-feature"), function(i,item) {
        var newMargin3 = -1 * (jQuery(window).width() - jQuery('#bp-article-content').width()) / 2;
        jQuery(item).css('height', divHeight16x9);
        jQuery(item).parent().css('height', divHeight16x9);
        jQuery(item).parent().parent().css('height',divHeight16x9);
        //jQuery(item).css('margin-left', newMargin3);
    });
    jQuery.each(jQuery("div.article-item-media"), function(i,item) {
        jQuery(item).css('height', jQuery(item).width() * 0.5625);
    });
}

jQuery(window).resize(function () {
    resizeFullWidthImages();
});

jQuery(document).ready(function () {
    count = 1;
    jQuery.each(jQuery(".video-full-feature"), function (i, item) {
        jQuery(item).attr('id', jQuery(item).attr('id') + "_" + count);
        count++;
    });

    jQuery(".commentsAnchor").click(function () {
        window.scrollTo(0, jQuery('#comment-area').offset().top);
    });
    jQuery(".bannerScroll").click(function () {
        jQuery('html, body').animate({ scrollTop: (jQuery('#main-story').offset().top) + "px" }, 800);
    });

    //code for full-width images                        
    resizeFullWidthImages();

    //banner switch for video
    jQuery(".banner_video_button").click(function () {
        jQuery(this).closest('.youtube-banner').css('cssText','height: auto !important;')
                    .find(".section-overlay, .text-overlay-wrapper, .article-banner-wrapper").hide();
        jQuery(this).closest('.youtube-banner').find(".video-banner-wrapper").fadeIn("slow");
        jQuery('#ytplayer_' + jQuery(this).attr('name'))[0].src = jQuery('#ytplayer_' + jQuery(this).attr('name'))[0].src.replace("autoplay=0", "autoplay=1");
    });

    jQuery(".mute-button-banner, .mute-button").click(function () {

        if (jQuery(this).parent().parent().prevAll('.img-full-feature').children("video").prop('muted') == true) {
            jQuery(this).parent().parent().prevAll('.img-full-feature').children("video").prop('muted', false);
            jQuery(this).addClass('mute');
            jQuery(this).text('Mute');
        }
        else {
            jQuery(this).parent().parent().prevAll('.img-full-feature').children("video").prop('muted', true);
            jQuery(this).removeClass('mute');
            jQuery(this).text('Listen');
        }
    });

    jQuery(window).scroll(function (event) {
        var scroll = jQuery(window).scrollTop();
        jQuery.each(jQuery(".video-full-feature,.banner-video-full-feature"), function (i, item) {
            var vid = document.getElementById(jQuery(item).attr('id'));
            if ((scroll + jQuery(window).height() >= jQuery(item).offset().top) && (scroll < jQuery(item).offset().top + jQuery(item).height())) {
                vid.play();
            }
            else {
                vid.pause();
            }
        });
    });

    //search modal functions
    jQuery(function () {
        jQuery('.search-modal').on('shown.bs.modal', function () {
            jQuery('#search-modal-text').focus();
        });
    });
    jQuery(".form-search").keypress(function (e) {
        if (e.which == 13) {
            jQuery(".form-search .btn").click();
        }
    });
    
    //end search modal
    
    //add classes to the global nav to allow for handeling of curved edges at end
    jQuery("#bp-subnav-global-nav ul.dropdown-menu:last").parent(".dropdown").addClass('last-menu-item');
    jQuery("#bp-subnav-global-nav").children().last().children(".dropdown-menu").addClass('dropdown-menu-right');
});
