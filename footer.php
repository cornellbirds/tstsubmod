

			<section class="bottom-widgets wrap clearfix">
            <?php birdpress_sitemap('site-map' );?>
            </section>
            <!-- footer -->
			<footer class="footer-wrap container-fluid">
	            <div class="foot">
	            <div class="row">
								<div class="cu-logo orn-logo">
									<a href="http://www.birds.cornell.edu/" class="lab-link"></a>
									<object type="image/svg+xml" data="<?php echo get_template_directory_uri() ?>/img/logo-clo--short.svg" class="svg-logo">The Cornell Lab of Ornithology</object>
								</div>
							<?php if(get_theme_mod('bp_cornell_logo', '') != 'no') { ?>
	            <div class="cu-logo">
		            <a href="http://www.cornell.edu/"><img src="<?php echo get_template_directory_uri(); ?>/img/cornell-logo.png" alt="Cornell Logo" /></a>
							</div>
							<?php } ?>
							<div class="cu-logo custom-footer-area">
								<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('custom-footer-widget') ) : ?>
								<div class="custom-footer-widget">

								</div>
								<?php endif; ?>
							</div>
	            <div class="contact-info">
  		            <ul>
                      <?php if(get_theme_mod('bp_twitter_link', '') != '') { ?>
      		            <li><a href="<?php echo get_theme_mod('bp_twitter_link', '')?>" target="_self"><span class="fa fa-twitter-square social-fix"></span></a></li>
                        <?php } ?>
                        <?php if(get_theme_mod('bp_facebook_link', '') != '') { ?>
		                <li><a href="<?php echo get_theme_mod('bp_facebook_link', '')?>" target="_self"><span class="fa fa-facebook-square social-fix"></span></a></li>
                        <?php } ?>
                        <?php if(get_theme_mod('bp_instagram_link', '') != '') { ?>
		                <li><a href="<?php echo get_theme_mod('bp_instagram_link', '')?>" target="_self"><span class="fa fa-instagram social-fix"></span></a></li>
                        <?php } ?>
  		            </ul>
		            <?php birdpress_footer('footer-menu'); ?>
		            <small>SILLY UPDATE NUMBER 3 Copyright &copy; <?php echo date("Y"); ?> Cornell University</small>
	            </div>
	            </div>
	            </div>
            </footer>
			<!-- /footer -->

		<!-- /wrapper -->

		<?php wp_footer(); ?>
		<?php
		include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
		if (is_plugin_active('wp-google-search/wp-google-search.php')) { ?>
		<script type="text/javascript">
			jQuery(".form-search .btn").click(function () {
        	window.location.href = 'search_gcse/?q=' + jQuery('#search-modal-text').val();
    	});
		</script>
		<?php } else { ?>
		<script type="text/javascript">
			jQuery(".form-search .btn").click(function () {
				<?php $searchurl = get_bloginfo('url') . '/?s='; ?>
        	window.location.href = '<?php echo $searchurl ?>' + jQuery('#search-modal-text').val();
    	});
		</script>
		<?php } ?>

	</body>
</html>
