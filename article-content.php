
<div id="bp-article-content">
    <?php 
    $banner_image = null;
    $banner_video = null;
    $banner_youtube = null;            
    $banner_image = get_post_meta( get_the_ID(), 'banner-image', true);
    $banner_video = null;    
    $banner_youtube = get_post_meta( get_the_ID(), 'banner-youtube', true);   
    if( !empty( $banner_image ) ) {         
        $custom_byline = get_post_meta( get_the_ID(), 'custom-byline', true );
        $banner_text_style = get_post_meta( get_the_ID(), 'banner-text-style', true );
        $banner_text_style = $banner_text_style . ' narrower-height top-banner';
        if( !empty( $banner_youtube ) ) {
            $banner_text_style .= ' youtube-banner';
        }
        $banner_caption = get_post_meta( get_the_ID(), 'banner-image-caption', true);
        if(substr($banner_image, strlen($banner_image) - 4) == ".mp4" || substr($banner_image, strlen($banner_image) - 4) == '.m4v')
        {                
            $banner_video = $banner_image;
            $banner_image = get_post_meta( get_the_ID(), 'fallback-videobanner-image', true );
        }
        $date = get_the_date();
        if(get_post_meta(get_the_ID(), 'bp-show-date', true) == 'no'){
            $date = '';
        }
        echo output_fullbleed_banner($banner_image, $banner_video, $banner_youtube, $banner_text_style, $custom_byline, $date, get_the_title(), $banner_caption, true);
    }
    ?>

    <?php if( empty($banner_image) ) { ?>
            
            
	    <div class="">            
			<h1><?php the_title(); ?></h1>
            <?php 
                $custom_byline = get_post_meta( get_the_ID(), 'custom-byline', true );
                $show_date = get_post_meta(get_the_ID(), 'bp-show-date', true);
                if( !empty($custom_byline) || ($show_date != 'no') ){
                    echo "<h5>";
                    if( !empty( $custom_byline ) ) {
                        echo $custom_byline . " " ;
                    }
                    if($show_date != 'no'){ 
                        the_date();
                    }
                    echo "</h5>";
                }
            ?>
	    </div>						
    <?php }
        birdpress_sharing();
        ?>
        
        <div id="main-story">

		<!-- Featured Photo Section -->
        <?php 
        $displayFeaturedImage = get_post_meta( get_the_ID(), 'featured-image-display', true );
        if ($displayFeaturedImage != "no"){ ?>
        <?php if ( has_post_thumbnail() ) : ?>
        <div class="photo medium">            
			<?php echo aab_get_image_tag( get_post_thumbnail_id( get_the_ID() ), 'medium' )  ?>
            <small><?php echo get_posts(array('p' => get_post_thumbnail_id( get_the_ID() ), 'post_type' => 'attachment'))[0]->post_excerpt ?></small>			                                                       
        </div>
        <?php endif; ?> 
        <?php } ?>
        <!--/ End Featured Photo Section -->
                        
 
        <?php 
        echo apply_filters("the_content",get_the_content());                    
        ?>   
        <!-- Comment Area -->
        </div>
           
</div>