<?php
/**
 * Template Name: Birdpress Search Results Page
 *
 *
 */

get_header();
?>
<div class="wrap content col-2 clearfix">
	<article class="grid" role="article">
    <h1>
    <?php echo sprintf( __( '%s ' . birdpress_search_results_text() . ' ', 'birdpress' ), $wp_query->found_posts ); echo "'" . get_search_query() . "'"; ?>
    </h1>
        <?php
        get_template_part('loop');
		get_template_part('pagination', $wp_query->max_num_pages);
        ?>
    </article>
    <aside class="sidebar">
    <?php get_sidebar()?>
    </aside>
</div>
<?php get_template_part('next-prev'); ?>
<?php get_template_part('comment-area'); ?>
<?php get_footer();?>
