<?php
/**
 * Template Name: Birdpress One Column Template
 *
 *
 */
get_header(); ?>
<div class="wrap content col-1 clearfix">
	<?php get_template_part('subnav'); ?>
    <article class="grid" id="bp-article-content" role="article">
        <?php
        if (have_posts()):
            while (have_posts()) :
                the_post();
                    ?>
                    <?php
                    $banner_image = null;
                    $banner_video = null;
                    $banner_youtube = null;
                    $banner_image = get_post_meta( get_the_ID(), 'banner-image', true);
                    $banner_video = null;
                    $banner_youtube = get_post_meta( get_the_ID(), 'banner-youtube', true);
                    if( !empty( $banner_image ) ) {
                        $custom_byline = get_post_meta( get_the_ID(), 'custom-byline', true );
                        $banner_text_style = get_post_meta( get_the_ID(), 'banner-text-style', true );
                        $banner_text_style = $banner_text_style . ' narrower-height top-banner';
                        if( !empty( $banner_youtube ) ) {
                            $banner_text_style .= ' youtube-banner';
                        }
                        $banner_caption = get_post_meta( get_the_ID(), 'banner-image-caption', true);
                        if(substr($banner_image, strlen($banner_image) - 4) == ".mp4" || substr($banner_image, strlen($banner_image) - 4) == '.m4v')
                        {
                            $banner_video = $banner_image;
                            $banner_image = get_post_meta( get_the_ID(), 'fallback-videobanner-image', true );
                        }
                        $date = get_the_date();
                        if(get_post_meta(get_the_ID(), 'bp-show-date', true) == 'no'){
                            $date = '';
                        }
                        echo output_fullbleed_banner($banner_image, $banner_video, $banner_youtube, $banner_text_style, $custom_byline, $date, get_the_title(), $banner_caption, true);
                    }
                    else {
                    ?>
                    <h1><?php the_title(); }?></h1>
                    <?php birdpress_sharing();
                    $displayFeaturedImage = get_post_meta( get_the_ID(), 'featured-image-display', true );
                    if ($displayFeaturedImage != "no"){ ?>
                    <?php if ( has_post_thumbnail() ) : ?>
                    <div class="photo medium">
                        <?php echo aab_get_image_tag( get_post_thumbnail_id( get_the_ID() ), 'medium' )  ?>
                        <small><?php echo get_posts(array('p' => get_post_thumbnail_id( get_the_ID() ), 'post_type' => 'attachment'))[0]->post_excerpt ?></small>
                    </div>
                    <?php endif;
                    }
                the_content();
            endwhile;
        endif;
        ?>
	</article>
</div>
<?php get_template_part('next-prev'); ?>
<?php get_template_part('comment-area'); ?>
<?php get_footer(); ?>
