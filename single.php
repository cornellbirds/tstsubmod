<?php
get_header();
?>
<?php
    if (have_posts()):
        while (have_posts()) : the_post(); ?>
        <?php
        $post_format = (get_post_meta(get_the_ID(),'bp-post-format',true)) ? get_post_meta(get_the_ID(),'bp-post-format',true) : 'col-2';
        ?>
		<div class="wrap content <?php echo $post_format ?> clearfix">
		    <!-- content -->
            <?php get_template_part('subnav'); ?>
			   <article class="grid" role="article">
            <?php get_template_part('article-content'); ?>
          </article>
            <?php if($post_format == 'col-2') { ?>
            <aside class="sidebar">
            <?php get_sidebar();?>
            </aside>
            <?php } ?>
        </div><!--end wrap -->
        <?php get_template_part('next-prev'); ?>
        <?php get_template_part('comment-area'); ?>
	<?php endwhile; ?>

	<?php else: ?>
		<h1><?php _e( 'Sorry, nothing to display.', 'birdpress' ); ?></h1>
	<?php endif; ?>

<?php get_footer(); ?>
