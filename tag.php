<?php get_header(); ?>
<div class="wrap content col-2 clearfix">
	<article class="grid" role="article">

			<?php get_template_part('loop'); ?>

			<?php get_template_part('pagination'); ?>

	</article>
    <aside class="sidebar">
    <?php get_sidebar();?>
    </aside>
</div>
<?php get_footer(); ?>
