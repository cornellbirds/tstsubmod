<?php
/**
 * Template Name: Success Story Template
 *
 *
 */
get_header(); ?>
<div class="wrap content success-story clearfix">
    <?php get_template_part('subnav');   ?>
    <article class="grid" role="article">
        <?php
        if (have_posts()):
            while (have_posts()) :
                the_post();
                    ?>
                    <h1><?php the_title()?></h1>
                    <?php birdpress_sharing();
                    $displayFeaturedImage = get_post_meta( get_the_ID(), 'featured-image-display', true );
                    if ($displayFeaturedImage != "no"){ ?>
                    <?php if ( has_post_thumbnail() ) : ?>
                    <div class="photo medium">
                        <?php echo aab_get_image_tag( get_post_thumbnail_id( get_the_ID() ), 'medium' )  ?>
                        <small><?php echo get_posts(array('p' => get_post_thumbnail_id( get_the_ID() ), 'post_type' => 'attachment'))[0]->post_excerpt ?></small>
                    </div>
                    <?php endif;
                    }
                the_content();
            endwhile;
        endif;
        ?>
    </article>
</div>
<?php get_template_part('next-prev'); ?>
<?php get_template_part('comment-area'); ?>
<?php get_footer(); ?>
