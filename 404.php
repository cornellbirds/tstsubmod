<?php get_header(); ?>

	<div class="wrap content col-2 clearfix">
	<article class="grid" role="article">

			<!-- message -->
			<div id="post-404">

				<h1><?php _e( 'Page not found', 'birdpress' ); ?></h1>
				<h2>
					<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'birdpress' ); ?></a>
				</h2>

			</div>
			<!-- /message -->

		</article>
    <aside class="sidebar">
    <?php get_sidebar();?>
    </aside>
</div>
<?php get_footer(); ?>
